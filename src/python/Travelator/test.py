import unittest
import os, sys
sys.path.insert(0, os.path.abspath("../.."))
from python_logic import *

class TestAppInterface(unittest.TestCase):
    def setUp(self):
        self.startupinterface = StartupInterface
        self.validators = Validators
        self.destinationinterface = DestinationInterface
        self.eventsinterface = Events
        self.entertainmentinterface = EntertainmentsInterface
        self.calendarinterface = CalendarInterface
        self.users = "test_users.csv"
        self.current = "test_current_user.csv"
        self.reviews = "test_destination_reviews.csv"
        self.destinations = "test_destinations.csv"
# User information tests
    def test_get_login_info(self):
        login_info = self.startupinterface.get_login_info(self.users)
        expected_output = [['Banani@gmail.com', 'Bananinn123'], ['Jakob1310@gmail.com', 'Unbreakable']]
        self.assertEqual(login_info, expected_output)

    def test_get_user_info(self):
        user_info = self.startupinterface.get_user_info(self.users)
        expected_output = [['Banani@gmail.com', 'Bananinn123', 'Banani', "33", "-"], ['Jakob1310@gmail.com', 'Unbreakable', 'Jakob1310', "-", "-"]]
        self.assertEqual(user_info, expected_output)
    
    def test_already_exists(self):
        already_exists = self.startupinterface.already_exists('Jakob1310@gmail.com')
        self.assertTrue(already_exists)
        already_exists = self.startupinterface.already_exists('Spaghetti@gmail.com')
        self.assertFalse(already_exists)

    def test_current_user(self):
        current_user = self.startupinterface.current_user(self.current)
        expected_output = 'Jakob1310@gmail.com'
        self.assertEqual(current_user, expected_output)

    def test_user_login(self):
        user_login_true = self.startupinterface.user_login('Jakob1310@gmail.com', 'Unbreakable', self.current, self.users)
        self.assertTrue(user_login_true)
        user_login_false = self.startupinterface.user_login('jakob1310@gmail.com', 'Unbreakable', self.current, self.users)
        self.assertFalse(user_login_false)
    
    def test_user_logout(self):
        user_logout = self.startupinterface.logout(self.current) # Logout just clears the user in the logged in file
        current_user = self.startupinterface.current_user(self.current) # Need to get the state of the current_user_file, should be empty
        expected_output = current_user
        self.assertEqual(current_user, expected_output)
        self.startupinterface.user_login('Jakob1310@gmail.com', 'Unbreakable', self.current)

# Account information tests
    def test_create_account(self):
        create_account = self.startupinterface.create_account('Ananas@gmail.com', 'Ananasinn123', 'Ananas', self.users)
        user_info = self.startupinterface.get_user_info(self.users)
        expected_output = [['Banani@gmail.com','Bananinn123','Banani', "33", "-"], ['Jakob1310@gmail.com','Unbreakable','Jakob1310', "-", "-"], ['Ananas@gmail.com', 'Ananasinn123', 'Ananas', "-", "-"]]
        self.assertEqual(user_info, expected_output)
        self.startupinterface.delete_account('Ananas@gmail.com', self.users)

    def test_delete_account(self):
        delete_account = self.startupinterface.delete_account('Jakob1310@gmail.com', self.users)
        user_info = self.startupinterface.get_user_info(self.users)
        expected_output = [['Banani@gmail.com','Bananinn123','Banani', "33", "-"]]
        self.assertEqual(user_info, expected_output)
        create_account = self.startupinterface.create_account('Jakob1310@gmail.com','Unbreakable','Jakob1310', self.users)

    def test_change_username(self):
        email = "Jakob1310@gmail.com"
        new_username = "JakobMatti"
        self.startupinterface.change_username(email, new_username, self.users)
        updated_user_info = self.startupinterface.get_user_info(self.users)
        expected_updated_info = [["Banani@gmail.com", "Bananinn123", "Banani", "33", "-"], 
                                 ["Jakob1310@gmail.com", "Unbreakable", "JakobMatti", "-", "-"]]
        self.assertEqual(updated_user_info, expected_updated_info)

    def test_change_username(self):
        email = "Jakob1310@gmail.com"
        new_username = "Jakob1310"
        self.startupinterface.change_username(email, new_username, self.users)
        updated_user_info = self.startupinterface.get_user_info(self.users)
        expected_updated_info = [["Banani@gmail.com", "Bananinn123", "Banani", "33", "-"], 
                                 ["Jakob1310@gmail.com", "Unbreakable", "Jakob1310", "-", "-"]]
        self.assertEqual(updated_user_info, expected_updated_info)

    def test_change_password(self):
        email = "Jakob1310@gmail.com"
        new_password = "Unbreakable123"
        self.startupinterface.change_password(email, new_password, self.users)
        updated_user_info = self.startupinterface.get_login_info(self.users)
        expected_updated_info = [["Banani@gmail.com", "Bananinn123"], ["Jakob1310@gmail.com", "Unbreakable123"]]
        self.assertEqual(updated_user_info, expected_updated_info)

    def test_change_password(self):
        email = "Jakob1310@gmail.com"
        new_password = "Unbreakable"
        self.startupinterface.change_password(email, new_password, self.users)
        updated_user_info = self.startupinterface.get_login_info(self.users)
        expected_updated_info = [["Banani@gmail.com", "Bananinn123"], ["Jakob1310@gmail.com", "Unbreakable"]]
        self.assertEqual(updated_user_info, expected_updated_info)

# Validation Tests
    def test_validate_email(self):
        email = "Danni@gmail.com"
        self.assertTrue(self.validators.validate_email(email))
        email = "Gabbi@gmail.kom"
        self.assertFalse(self.validators.validate_email(email))
        email = "gabbi.is"
        self.assertFalse(self.validators.validate_email(email))
        email = "@.is"
        self.assertFalse(self.validators.validate_email(email))
        email = "GabrielManiErBesturIOllumHeiminum@gmail.com"
        self.assertFalse(self.validators.validate_email(email))
        email = "Gabriel@gmailcom"
        self.assertFalse(self.validators.validate_email(email))

    def test_validate_password(self):
        password = "Gabbi"
        self.assertFalse(self.validators.validate_password(password))
        password = "gabbi"
        self.assertFalse(self.validators.validate_password(password))
        password = "GabrielMani123"
        self.assertTrue(self.validators.validate_password(password))
        password = "gabrielmani123"
        self.assertFalse(self.validators.validate_password(password))
        password = "GabrielManiErBesturIOllumHeiminum1234"
        self.assertFalse(self.validators.validate_password(password))

    def test_validate_username(self):
        name = "Ra"
        self.assertFalse(self.validators.validate_username(name))
        name = "Osiris"
        self.assertTrue(self.validators.validate_username(name))
        name = "GabrielManiErBesturIOllumHeiminum123"
        self.assertFalse(self.validators.validate_username(name))

# Tests for Destinations
    def test_get_destinations(self):
        input = "1"
        expected_output = 'Berlin,Germany,52.5200/13.4050,0,5!London,United Kingdom,51.5074/-0.1278,0,3!New York,United States,40.7128/-74.0060,0,1!Paris,France,48.8566/2.3522,0,4!Toronto,Canada,43.6511/-79.3830,0,2'
        dests = self.destinationinterface.get_destinations(input, "test_destinations.csv")
        self.assertEqual(dests, expected_output)
        input = "2"
        expected_output = "Toronto,Canada,43.6511/-79.3830,0,2!Paris,France,48.8566/2.3522,0,4!Berlin,Germany,52.5200/13.4050,0,5!London,United Kingdom,51.5074/-0.1278,0,3!New York,United States,40.7128/-74.0060,0,1"
        dests = self.destinationinterface.get_destinations(input, "test_destinations.csv")
        self.assertEqual(dests, expected_output)
        input = "3-Toronto"
        expected_output = "Toronto,Canada,43.6511/-79.3830,0,2"
        dests = self.destinationinterface.get_destinations(input, "test_destinations.csv")
        self.assertEqual(dests, expected_output)
        input = "4"
        expected_output = "Berlin,Germany,52.5200/13.4050,0,5!London,United Kingdom,51.5074/-0.1278,0,3!New York,United States,40.7128/-74.0060,0,1!Paris,France,48.8566/2.3522,0,4!Toronto,Canada,43.6511/-79.3830,0,2"
        dests = self.destinationinterface.get_destinations(input, "test_destinations.csv")
        self.assertEqual(dests, expected_output)
        input = "5"
        expected_output = "London,United Kingdom,51.5074/-0.1278,0,3,1!New York,United States,40.7128/-74.0060,0,1,1!Berlin,Germany,52.5200/13.4050,0,5,0!Paris,France,48.8566/2.3522,0,4,0!Toronto,Canada,43.6511/-79.3830,0,2,0"
        dests = self.destinationinterface.get_destinations(input, "test_destinations.csv")
        self.assertEqual(dests, expected_output)

    def test_add_and_delete_favorite_destination(self):
        #Add and delete are tested at the same time as to not interfere with other tests.
        emailinput = "Banani@gmail.com"
        destinput = "6"
        self.destinationinterface.add_favorite_destination(emailinput, destinput, self.users)
        expected_updated_info = [["Banani@gmail.com", "Bananinn123", "Banani", "33-6", "-"],["Jakob1310@gmail.com", "Unbreakable", "Jakob1310", "-", "-"]]
        user_info = self.startupinterface.get_user_info(self.users)
        self.assertEqual(user_info, expected_updated_info)
        emailinput = "Jakob1310@gmail.com"
        destinput = "6"
        self.destinationinterface.add_favorite_destination(emailinput, destinput, self.users)
        expected_updated_info = [["Banani@gmail.com", "Bananinn123", "Banani", "33-6", "-"],["Jakob1310@gmail.com", "Unbreakable", "Jakob1310", "6", "-"]]
        user_info = self.startupinterface.get_user_info(self.users)
        self.assertEqual(user_info, expected_updated_info)
        emailinput = "Banani@gmail.com"
        destinput = "6"
        self.destinationinterface.delete_favorite_destination(emailinput, destinput, self.users)
        expected_updated_info = [["Banani@gmail.com", "Bananinn123", "Banani", "33", "-"],["Jakob1310@gmail.com", "Unbreakable", "Jakob1310", "6", "-"]]
        user_info = self.startupinterface.get_user_info(self.users)
        self.assertEqual(user_info, expected_updated_info)
        emailinput = "Jakob1310@gmail.com"
        destinput = "6"
        self.destinationinterface.delete_favorite_destination(emailinput, destinput, self.users)
        expected_updated_info = [["Banani@gmail.com", "Bananinn123", "Banani", "33", "-"],["Jakob1310@gmail.com", "Unbreakable", "Jakob1310", "-", "-"]]
        user_info = self.startupinterface.get_user_info(self.users)
        self.assertEqual(user_info, expected_updated_info)
    
# Tests for Events
    def test_get_events(self):
        input = '1-14'
        expected_output = 'Name             Price            Starting Time    Finishing Time   Date             Accessibility--Some Name        $155             12:15            16:27            7/5/2023         -1-Some Name        $100             12:15            13:27            10/2/2023        Wheelchair Accessible-9-Some Name        $140             13:15            17:28            8/5/2023         -87-Some Name        $135             10:15            14:25            13/1/2023        Alcohol Available-247-'
        events = self.eventsinterface.get_events(input)
        self.assertEqual(events, expected_output)
        input = "2-135-135-14"
        expected_output = 'Name             Price            Starting Time    Finishing Time   Date             Accessibility--Some Name        $135             10:15            14:25            13/1/2023        Alcohol Available-247-'
        events = self.eventsinterface.get_events(input)
        self.assertEqual(events,expected_output)
        input = "3-140-14"
        expected_output = 'Name             Price            Starting Time    Finishing Time   Date             Accessibility--Some Name        $140             13:15            17:28            8/5/2023         -87-'
        events = self.eventsinterface.get_events(input)
        input = "4-Alcohol Available-14"
        expected_output = 'Name             Price            Starting Time    Finishing Time   Date             Accessibility--Some Name        $135             10:15            14:25            13/1/2023        Alcohol Available-247-'
        events = self.eventsinterface.get_events(input)
        self.assertEqual(events,expected_output)
        input = "1-13"
        expected_output = 'This City Has No Events.'
        events = self.eventsinterface.get_events(input)
        self.assertEqual(events,expected_output)
        input = "2-0-0-14"
        expected_output = 'No events in price range.'
        events = self.eventsinterface.get_events(input)
        self.assertEqual(events,expected_output)
        input = "3-0-14"
        expected_output = 'No events equal to price.'
        events = self.eventsinterface.get_events(input)
        self.assertEqual(events,expected_output)
        input = "4-Kid Friendly-14"
        expected_output = 'No events with chosen filter.'
        events = self.eventsinterface.get_events(input)
        self.assertEqual(events,expected_output)


# Tests for Entertainments
    def test_get_entertainments(self):
        input = "1-4"
        expected_output = "Name             Price            Accessibility--Some Name        $150             Wheelchair Accessible & Alcohol Available-135-"
        entertainments = self.entertainmentinterface.get_entertainments(input)
        self.assertEqual(entertainments, expected_output)
        input = "2-130-170-4"
        expected_output = "Name             Price            Accessibility--Some Name        $150             Wheelchair Accessible & Alcohol Available-135-"
        entertainments = self.entertainmentinterface.get_entertainments(input)
        self.assertEqual(entertainments, expected_output)
        input = "3-100-4"
        expected_output = "No entertainments equal to price."
        entertainments = self.entertainmentinterface.get_entertainments(input)
        self.assertEqual(entertainments, expected_output)
        input = "3-150-4"
        expected_output = "Name             Price            Accessibility--Some Name        $150             Wheelchair Accessible & Alcohol Available-135-"
        entertainments = self.entertainmentinterface.get_entertainments(input)
        self.assertEqual(entertainments, expected_output)

# Tests for Reviews   
    def get_destination_reviews(self):
        destination_reviews = self.destinationinterface.get_destination_reviews(self.reviews)
        expected_output = [['Jakob1310@gmail.com', '3', 'London has big ben; the London Eye and Thorpe Park.', 'Jakob1310', '4'], ['Jakob1310@gmail.com', '2', 'Toronto really is cold.', 'Jakob1310', '4'], ['Jakob1310@gmail.com', '4', "Paris is hot; but sometimes it's cold.", 'Jakob1310', '3'], ['Banani@gmail.com', '3', "London is hot; but sometimes it's even hotter!.", 'Banani', '3']]
        self.assertEqual(destination_reviews, expected_output)


    def test_get_reviews_on_destination(self):
        reviews_on_destination = self.destinationinterface.get_reviews_on_destination("3", self.reviews)
        expected_output = "Jakob1310@gmail.com,3,London has big ben; the London Eye and Thorpe Park.,Jakob1310,4!-!_!Banani@gmail.com,3,London is hot; but sometimes it's even hotter!.,Banani,3"
        self.assertEqual(reviews_on_destination, expected_output)


    def test_get_user_reviews(self):
        user_reviews = self.destinationinterface.get_user_reviews("Jakob1310@gmail.com", self.reviews)
        expected_output = "Jakob1310@gmail.com,3,London has big ben; the London Eye and Thorpe Park.,Jakob1310,4!-!_!Jakob1310@gmail.com,2,Toronto really is cold.,Jakob1310,4!-!_!Jakob1310@gmail.com,4,Paris is hot; but sometimes it's cold.,Jakob1310,3"
        self.assertEqual(user_reviews, expected_output)


    def test_review_already_exists(self):
        review_exists = self.destinationinterface.review_already_exists("Jakob1310@gmail.com", "3", self.reviews)
        expected_output = True # Exists
        self.assertEqual(review_exists, expected_output)
        review_exists = self.destinationinterface.review_already_exists("Banani@gmail.com", "3", self.reviews)
        expected_output = True # Exists
        self.assertEqual(review_exists, expected_output)

        review_exists = self.destinationinterface.review_already_exists("Jakob1310@gmail.com", "100", self.reviews)
        expected_output = False # Does not exist
        self.assertEqual(review_exists, expected_output)
        review_exists = self.destinationinterface.review_already_exists("Jakob1310@gmail.co", "41", self.reviews)
        expected_output = False # Does not exist
        self.assertEqual(review_exists, expected_output)


    def test_make_review(self):
        self.assertTrue(self.destinationinterface.make_review("Jakob1310@gmail.com","5","Now this city is a fabulous one, I mean really amazing","4",self.reviews, self.users, self.destinations))
        self.destinationinterface.delete_review("Jakob1310@gmail.com", "5", self.destinations, self.reviews)


    def test_edit_review(self):
        self.assertTrue(self.destinationinterface.edit_review("Jakob1310@gmail.com","3","Edited review, shabang","3", self.reviews, self.destinations))
        self.destinationinterface.edit_review("Jakob1310@gmail.com","3","London has big ben; the London Eye and Thorpe Park.","4", self.reviews, self.destinations)


    def test_delete_review(self):
        self.assertTrue(self.destinationinterface.delete_review("Jakob1310@gmail.com", "3", self.destinations, self.reviews)) # Deleting all reviews
        self.assertTrue(self.destinationinterface.delete_review("Jakob1310@gmail.com", "2", self.destinations, self.reviews))
        self.assertTrue(self.destinationinterface.delete_review("Jakob1310@gmail.com", "4", self.destinations, self.reviews))
        self.assertTrue(self.destinationinterface.delete_review("Banani@gmail.com", "3", self.destinations, self.reviews))

        self.destinationinterface.make_review("Jakob1310@gmail.com","3","London has big ben; the London Eye and Thorpe Park.","4", self.reviews, self.users, self.destinations) # Adding them back in correct order
        self.destinationinterface.make_review("Jakob1310@gmail.com","2","Toronto really is cold.", "4", self.reviews, self.users, self.destinations)
        self.destinationinterface.make_review("Jakob1310@gmail.com","4","Paris is hot; but sometimes it's cold.", "3", self.reviews, self.users, self.destinations)
        self.destinationinterface.make_review("Banani@gmail.com","3","London is hot; but sometimes it's even hotter!.", "3", self.reviews, self.users, self.destinations)

# Tests for Calendar
    def test_add_to_calendar(self):
        self.calendarinterface.add_to_calendar("Some Name#!#Fun Time", "Event", "y", "$180", "12/8/2023", "13:15", "14:28", "Jakob1310@gmail.com", "17", "12")
        output = self.calendarinterface.get("2023", "8", "12", "Jakob1310@gmail.com")
        expected_output = "Starting Time    Finishing Time   Price    Type             Description\n13:15            14:28            $180     Event            Fun Time_!##_12"
        self.assertEqual(output, expected_output)
        self.calendarinterface.add_to_calendar("Some Name#!#Darts", "Entertainment", "y", "$90", "13/12/2023", "15:00", "17:00", "Jakob1310@gmail.com", "17", "5")
        output = self.calendarinterface.get("2023", "12", "13", "Jakob1310@gmail.com")
        expectedoutput = "Starting Time    Finishing Time   Price    Type             Description\n15:00            17:00            $90     Entertainment            Darts_!##_12"

    def test_remove_from_calendar(self):
        self.calendarinterface.remove_from_calendar("13:15            14:28            $180     Event    Fun Time", "Jakob1310@gmail.com", "2023-8-12")
        output = self.calendarinterface.get("2023", "8", "12", "Jakob1310@gmail.com")
        expected_output = "Starting Time    Finishing Time   Price    Type             Description_!##_"
        self.assertEqual(output, expected_output)
        self.calendarinterface.remove_from_calendar("15:00            17:00            $90     Entertainment    Darts", "Jakob1310@gmail.com", "2023-12-13")
        output = self.calendarinterface.get("2023", "12", "13", "Jakob1310@gmail.com")
        expectedoutput = "Starting Time    Finishing Time   Price    Type             Description_!##_"

    def test_get_month(self):
        month = self.calendarinterface.get_month("2300", "1", "Jakob1310@gmail.com")
        expected_output = "                                                                January 2300                                                                \n       Monday             Tuesday            Wednesday            Thursday             Friday             Saturday             Sunday       \n         1                   2                   3                   4                   5                   6                   7          \n         8                   9                   10                  11                  12                  13                  14         \n         15                  16                  17                  18                  19                  20                  21         \n         22                  23                  24                  25                  26                  27                  28         \n         29                  30                  31         "
        self.assertEqual(month, expected_output)

if __name__ == "__main__":
    unittest.main()
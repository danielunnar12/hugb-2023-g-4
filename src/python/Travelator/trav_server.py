from concurrent import futures
import os, sys
sys.path.insert(0, os.path.abspath("../.."))
from python_logic import *

import grpc
from python import login_pb2
from python import login_pb2_grpc
from python import createaccount_pb2
from python import createaccount_pb2_grpc
from python import changepass_pb2
from python import changepass_pb2_grpc
from python import changeusername_pb2
from python import changeusername_pb2_grpc
from python import destination_pb2
from python import destination_pb2_grpc
from python import events_pb2
from python import events_pb2_grpc
from python import favoritedestination_pb2
from python import favoritedestination_pb2_grpc
from python import entertainments_pb2
from python import entertainments_pb2_grpc
from python import calendar_pb2
from python import calendar_pb2_grpc

"""
gRPC takes input from a client and returns some message back to the server depending on the function
   - request.some_variable: Fetches some input from the client depending on the proto file designated to the servicer
   - return some_pb2.someOutput(message): Returns a message to the client depending on the proto file designated to the servicer
"""

class Login(login_pb2_grpc.LoginServicer):
   """Login servicer"""
   def login(self, request, context):
      """Recieves email input and password input for the login from client and logs the user in"""
      print("Got login info request " + str(request))
      email = request.email
      password = request.password
      if Validators.validate_email(email) == True:
         if StartupInterface.user_login(email, password) == True:
            return login_pb2.ConfirmationOutput(message='{0}'.format("k")) # If the login is successfull then it sends "k" back to kill the auth client and open the main client
         else:
            print("No matching email and password pair error")
            return login_pb2.ConfirmationOutput(message='{0}'.format("err2"))
      else:
         print("Invalid email error")
         return login_pb2.ConfirmationOutput(message='{0}'.format("err1"))
   
class CreateAccount(createaccount_pb2_grpc.CreateAccountServicer):
   """Account creation servicer"""
   def create_account(self, request, context):
      """Recieves email,password,username inputs from the client and creates the account
      
      Args:
         email (str): Email(ID) for the new user
         password (str): Password to be used for login
         username (str): A display name for the user
      
      Returns:
         message (str): A message to tell the client what to do depending on what happened in the server
      """
      print("Got user information " + str(request))
      email = request.email
      password = request.password
      username = request.username
      if Validators.validate_email(email) == True:  # Checks if the email contains 1 @ symbol and 1 ending(.com, .is, .net)
         if Validators.validate_password(password) == True:
            if Validators.validate_username(username) == True:
               if StartupInterface.already_exists(email) == True:  # Checks if the email already exists
                  print("Email exists error")
                  return createaccount_pb2.AccountInfoOutput(message='{0}'.format("err4"))
               else:
                  StartupInterface.create_account(email, password, username)
                  print("Server created account")
                  return createaccount_pb2.AccountInfoOutput(message='{0} {1} {2} {3}'.format("c", email, password, username))
            else:
               print("Invalid username error")
               return createaccount_pb2.AccountInfoOutput(message='{0}'.format("err3"))
         else:
            print("Invalid password error")
            return createaccount_pb2.AccountInfoOutput(message='{0}'.format("err2"))
      else:
         print("Invalid email error")
         return createaccount_pb2.AccountInfoOutput(message='{0}'.format("err1"))
   
   
   def delete_account(self, request, context):
      """Recieves email,password,confirm_password inputs from the client and deletes the account
      
      Args:
         email (str): Email(ID) for the new user
         password (str): Password to be used for login
         confirm_password (str): Must match password to complete account deletion
      
      Returns:
         message (str): A message to tell the client what to do depending on what happened in the server
      """
      print("Got deletion request with current password")
      password = request.password
      user = request.email
      confirm_password = request.confirm_password
      if StartupInterface.user_login(user, password) == True:
         if password == confirm_password:
            StartupInterface.logout()
            StartupInterface.delete_account(user)
            print("Account deletion complete")
            return createaccount_pb2.DeletionOutput(message='{0}'.format("k"))
         else:
            print("Error: Passwords did not match")
            return createaccount_pb2.DeletionOutput(message='{0}'.format("err2"))
      else:
         print("Error: Incorrect password")
         return createaccount_pb2.DeletionOutput(message='{0}'.format("err1"))

class ChangePass(changepass_pb2_grpc.ChangePassServicer):
   """Password change servicer"""
   def check_current_pass(self, request, context):
      """Recieves email,password,username inputs from the client and confirms the current password
      
      Args:
         email (str): Email(ID) for the new user
         password (str): Password to be used for login
      
      Returns:
         message (str): A message to tell the client if the email and password combination exists
      """
      print("Got check_password request info" + str(request))
      email = request.email
      password = request.currpass
      if StartupInterface.user_login(email, password) == True:
         return changepass_pb2.CurrPassOutput(message='{0}'.format(True))
      else:
         return changepass_pb2.CurrPassOutput(message='{0}'.format(False))
   
   
   def change_pass(self, request, context):
      """Recieves password input from the client and validates the password
      
      Args:
         new_password (str): New password to be used for login
      
      Returns:
         message (str): A message to tell the client what to do depending on what happened in the server
      """
      print("Got new password" + str(request))
      new_password = request.newpass 
      validate = Validators.validate_password(new_password)
      if validate == True:
         return changepass_pb2.NewPassOutput(message='{0}'.format(request.newpass))
      else:
         return changepass_pb2.NewPassOutput(message='{0}'.format(False))
   
   
   def confirm_current_pass(self, request, context):
      """Recieves the new proposed password with confirm_pass to compare them and confirm the new password from the client and changes the password
      
      Args:
         user (str): The users Email(ID)
         confirm_pass (str): New password being entered for a second time to confirm the password change
         passtocompare (str): The new password that was entered first, compare the confirm pass to the passtocompare to see if they match.
      
      Returns:
         message (str): A message to tell the client what to do depending on what happened in the server
      """
      print("Got password request to confirm account deletion ")
      confirm_pass = request.confirmpass
      passtocompare = request.passtocompare
      user = request.user
      if confirm_pass == passtocompare:
         StartupInterface.change_password(user, passtocompare)
         return changepass_pb2.ConfirmPassOutput(message='{0}'.format(True))
      else:
         return changepass_pb2.ConfirmPassOutput(message='{0}'.format(False))
   
class ChangeName(changeusername_pb2_grpc.ChangeNameServicer):
   """Name change servicer"""
   def change_name(self, request, context):
      """Recieves new username input from the client and validates the new username
      
      Args:
         new_name (str): New username for account
      
      Returns:
         message (str): A message to tell the client what to do
      """
      print("Got new username" + str(request))
      new_name = request.newname 
      validate = Validators.validate_username(new_name)
      if validate == True:
         return changeusername_pb2.NewNameOutput(message='{0}'.format(request.newname))
      else:
         return changeusername_pb2.NewNameOutput(message='{0}'.format(False))
   

   def confirm_name(self, request, context):
      """Recieves new username and a second input which is suppose to match the new username from the client to compare them and confirm the name change
      
      Args:
         user (str): Users Email(ID)
         confirm_name (str): The new name entered for the second time which must be compared to the first new name input
         nametocompare (str): string to be compared to confirm_name
      
      Returns:
         message (str): A message to tell the client what to do
      
      """
      print("Got names to compare" + str(request))
      confirm_name = request.confname
      nametocompare = request.nametocompare
      user = request.email
      if confirm_name == nametocompare:
         StartupInterface.change_username(user, nametocompare)
         return changeusername_pb2.ConfirmNameOutput(message='{0}'.format(True))
      else:
         return changeusername_pb2.ConfirmNameOutput(message='{0}'.format(False))

class GetDestination(destination_pb2_grpc.GetDestinationServicer):
   """Destination servicer"""
   def get_destination(self, request, context):
      """Recieves a string which the logic will interpret and return the destinations in the style that the user had chosen
      
      Args:
         get_destination (str): Users Email(ID)
         dests (str): A list of lists made a string with information on all destinatinons
      Returns:
         dests (str)
      
      """
      print("Got get destination request" + str(request))
      get_destination = request.getdest 
      dests = DestinationInterface.get_destinations(get_destination)
      return destination_pb2.DestinationsOutput(message='{0}'.format(dests))
   
   def get_dest_review(self, request, context):
      """Creates a review for a destination chosen by the user
      
      Args:
         email (str): Email(ID) of the user
         dest_id (str): Destination ID
         review (str): Input from user formatted as a string
         rating (str): String in range 1-5 (1, 2, 3, 4 or 5)
         
      Returns:
         message (str): A message to tell the client what to do depending on what happened in the server
      
      """
      print("Got review request:" + str(request))
      email = request.email
      dest_id = request.dest_id
      review = request.review
      rating = request.rating
      completed = DestinationInterface.make_review(email, dest_id, review, rating)
      if completed == False:
         return destination_pb2.ReviewOutput(message='{0}'.format("n"))
      else:
         return destination_pb2.ReviewOutput(message='{0}'.format("y"))
   
   def edit_dest_review(self, request, context):
      """Edits an already existing review for a destination chosen by the user
      
      Args:
         email (str): Email(ID) of the user
         dest_id (str): Destination ID
         new_review (str): Input from user formatted as a string
         rating (str): String in range 1-5 (1, 2, 3, 4 or 5)
         
      Returns:
         message (str): A message to tell the client what to do depending on what happened in the server
      
      """
      print("Got edit review request:" + str(request))
      email = request.email
      dest_id = request.dest_id
      new_review = request.new_review
      rating = request.rating
      completed = DestinationInterface.edit_review(email, dest_id, new_review, rating)
      if completed == False:
         return destination_pb2.ReviewEditOutput(message='{0}'.format("n"))
      else:
         return destination_pb2.ReviewEditOutput(message='{0}'.format("y"))
   
   def get_reviews_on_dest(self, request, context):
      """Returns all reviews on a certain destination chosen by the user
      
      Args:
         dest_id (str): Destination ID
         
      Returns:
         message (str): Returns a string of all reviews for the chosen destination
      
      """
      print("Got reviews for destination request:" + str(request))
      dest_id = request.get_reviews
      reviews_on_dest = DestinationInterface.get_reviews_on_destination(dest_id)
      print(reviews_on_dest)
      return destination_pb2.GetReviewsOutput(message=reviews_on_dest)
   
   def get_user_reviews(self, request, context):
      """Returns all reviews on a certain destination chosen by the user
      
      Args:
         user (str): Email(ID) of the user
         
      Returns:
         message (str): Returns a string of all reviews the user has made on all locations
      
      """
      print("Got user reviews request:" + str(request))
      user = request.email
      user_reviews = DestinationInterface.get_user_reviews(user)
      if user_reviews == None:
         return destination_pb2.UserReviewsOutput(message="None")
      else:
         return destination_pb2.UserReviewsOutput(message=user_reviews)
   
   def delete_user_review(self, request, context):
      """Deletes a review that the user has made on a specific location
      
      Args:
         user (str): Email(ID) of the user
         dest_id (str): Destination ID
         
      Returns:
         message (str): Returns a message that tells the client what to do depending on what happened in the client
      
      """
      print("Got review deletion request:" + str(request))
      email = request.email
      dest_id = request.dest_id
      DestinationInterface.delete_review(email, dest_id)
      return destination_pb2.DelReviewOutput(message='{0}'.format("del"))

      
   
class GetEvents(events_pb2_grpc.GetEventsServicer):
   """Events Servicer"""
   def get_events(self, request, context):
      """Gets a string of events depending on the input of the user and the destination chosen
      
      Args:
         event (str): an input from the user formatted differently for each filter
      
      Returns:
         events (str): A string of events

      """
      #Print requests received, not too important here but in other cases will be important
      print("Got request "+ str(request))
      #Get the destinations from the app_interface2 file
      event = Events.get_events(request.eventinput)
      return events_pb2.ServerOutput(events=event)

class FavoriteDestination(favoritedestination_pb2_grpc.FavoriteDestinationServicer):
   """Favorites servicer"""
   def newfavorite(self, request, context):
      """Takes the email of the user and a destination id and stores it as a favorite for that user
      
      Args:
         email (str): an input from the user formatted differently for each filter
         newdest (str): ID of the destination to make favorite

      Returns:
         favoriteconfirmation(str): A string that says if the destination was made favorite or it had already been favorited

      """
      print("Got request"+ str(request))
      email = request.email
      newdest = request.favoritedest
      message = DestinationInterface.add_favorite_destination(email, newdest)
      return favoritedestination_pb2.FavoriteOutput(favoriteconfirmation=message)
   

   def getfavorites(self, request, context):
      """Gets all favorite destinations from the user
      
      Args:
         email (str): an input from the user formatted differently for each filter

      Returns:
         destid(str): a string of the users favorite destinations

      """
      print("Got request"+ str(request))
      email = request.email
      user_info = StartupInterface.get_user_info()
      for user in user_info:
         if user[0] == email:
            favorites = user[3]
      return favoritedestination_pb2.GetFavoriteOutput(destid=favorites)
   def deletefavortie(self, request, context):
      """Takes the email of the user and a destination id and deletes it from the favorite destinations of that user
      
      Args:
         email (str): an input from the user formatted differently for each filter
         olddest (str): ID of the destination to remove from favorites

      Returns:
         deletefavoriteconfirmation(str): A string that says if the destination was removed from favorites or not

      """
      print("Got request"+ str(request))
      email = request.email
      olddest = request.favoritedest
      DestinationInterface.delete_favorite_destination(email, olddest)
      return favoritedestination_pb2.DeleteFavoriteOutput(deletefavoriteconfirmation="Removed from favorites")
   
class Entertainments(entertainments_pb2_grpc.EntertainmentsServicer):
   """Entertainments servicer"""
   def get_entertainments(self, request, context):
      """Gets a string of entertainments depending on the input of the user and the destination chosen
      
      Args:
         event (str): an input from the user formatted differently for each filter
      
      Returns:
         events (str): A string of entertainments

      """
      #Print requests received
      print("Got request "+ str(request))
      ent = EntertainmentsInterface.get_entertainments(request.entmtinput)
      return entertainments_pb2.EntertainmentOutput(entertainments=ent)

class Calendar(calendar_pb2_grpc.CalendarServicer):
   def print_month(self, request, context):
      print("Got request "+str(request))
      calendar = CalendarInterface.get_month(request.year, request.month, request.user_id)
      return calendar_pb2.MonthOutput(month_output=calendar, year=request.year, month=request.month)

   def get_date_details(self, request, context):
      print("Got request "+str(request))
      details = CalendarInterface.get(request.year, request.month, request.date, request.user_id)
      return calendar_pb2.DateDetails(date_details=details)

   def add_to_calendar(self, request, context):
      print("Got request "+str(request))
      confirm = CalendarInterface.add_to_calendar(request.plan, request.type, request.check, request.price, request.date, request.startt, request.endt, request.user, request.dest_id, request.e_id)
      return calendar_pb2.PlanConfirmation(confirmation=confirm)

   def remove_from_calendar(self, request, context):
      print("Got request "+str(request))
      confirm = CalendarInterface.remove_from_calendar(request.selection, request.user, request.date)
      return calendar_pb2.DeletionConfirmation(confirmation=confirm)

   def wipe_calendar(self, request, context):
      print("Got request "+str(request))
      confirm = CalendarInterface.wipe_calendar(request.user)
      return calendar_pb2.WipeConfirmation(confirmation=confirm)

   
def server():
   server = grpc.server(futures.ThreadPoolExecutor(max_workers=2))
   login_pb2_grpc.add_LoginServicer_to_server(Login(), server)
   createaccount_pb2_grpc.add_CreateAccountServicer_to_server(CreateAccount(), server)
   changepass_pb2_grpc.add_ChangePassServicer_to_server(ChangePass(), server)
   changeusername_pb2_grpc.add_ChangeNameServicer_to_server(ChangeName(), server)
   destination_pb2_grpc.add_GetDestinationServicer_to_server(GetDestination(), server)
   events_pb2_grpc.add_GetEventsServicer_to_server(GetEvents(), server)
   favoritedestination_pb2_grpc.add_FavoriteDestinationServicer_to_server(FavoriteDestination(), server)
   entertainments_pb2_grpc.add_EntertainmentsServicer_to_server(Entertainments(), server)
   calendar_pb2_grpc.add_CalendarServicer_to_server(Calendar(), server)
   server.add_insecure_port('[::]:50051')
   print("gRPC starting")
   server.start()
   server.wait_for_termination()
server()
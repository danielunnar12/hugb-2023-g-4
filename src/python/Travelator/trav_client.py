import grpc
import os, sys
from getpass import getpass
sys.path.insert(0, os.path.abspath("../.."))
from python_logic import *

from python import login_pb2
from python import login_pb2_grpc
from python import createaccount_pb2
from python import createaccount_pb2_grpc
from python import changepass_pb2
from python import changepass_pb2_grpc
from python import changeusername_pb2
from python import changeusername_pb2_grpc
from python import destination_pb2
from python import destination_pb2_grpc
from python import events_pb2
from python import events_pb2_grpc
from python import favoritedestination_pb2
from python import favoritedestination_pb2_grpc
from python import weatherStation_pb2
from python import weatherStation_pb2_grpc
from python import entertainments_pb2
from python import entertainments_pb2_grpc
from python import calendar_pb2
from python import calendar_pb2_grpc

# Table of contents
#
# Main():
#   startup_client():
#   login_client():
#   account_creation_client():
#   authorization_client():
#   main_client()

# Account():
#   account_details_client():
#   delete_account_client():
#   change_username_client():
#   change_password_client():
#   view_favorites_client():
#   view_favorite_client():
#   user_reviews_client():

# Destination():
#   get_destinations_client():
#   get_destinations_client_choice():
#   View_destination_client

# Events():
#   StartEvents():
#   AllEvents():
#   EBetween():
#   EEqual():
#   EAccess():

# Entertainments():
#   StartEntertainments():
#   AllEntertainments():
#   EntBetweenPrices():
#   EntEqualtoPrice():
#   EntAccFilter():

# Reviews():
#   destination_reviews_client():
#   add_review_client():

# Calendar_client():
#   view_calendar_client_start():
#   view_calendar_client():
#   view_date_client():
#   event_planning_client():
#   entertainment_planning_client():
#   plan_client():
#   wipe_calendar_client():


class Main_client():
   def startup_client(selection = "start"):
      """Starts the authorization or main client depending if there is already a user logged in before hand."""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = login_pb2_grpc.LoginStub(channel)
         print("Welcome to Travelator! \U0001f600")
         while selection != "k":
            user = StartupInterface.current_user()
            if user == None:
               selection = Main_client.authorization_client()
            else:
               selection = Main_client.main_client(user)
         print("\nSafe travels \U0001f30D\U00002708")

   
   def authorization_client(selection = "start"):
      """Gives the user the option to login, create an account or kill the application"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = login_pb2_grpc.LoginStub(channel)
         while selection != "k":
            print("\n"*3,"---- No current user ----")
            print("\n"*3,"Options\n1. Log in\n2. Create account\nk. Kill\n")
            selection = str(input("Select: ")).lower()
            if selection != "1" and selection != "2" and selection != "k":
               print("Error: Invalid input")
            else:
               if selection == "1":
                  selection = Main_client.login_client()
               elif selection == "2":
                  selection = Main_client.account_creation_client()
               elif selection == "k":
                  '''kill client'''
                  return "k"
   

   def login_client():
      """Client to log in the user with email and password."""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = login_pb2_grpc.LoginStub(channel)

         # Takes in email and password as input, then goes into startupinterface class and creates the account there'''
         get_login_info = stub.login(login_pb2.UserInput(email=str(input("Please enter your email: ")), password = str(getpass("Please enter your password: "))))

         command = get_login_info.message.split(" ")[0]
         if command == "k":
            print("Logged in")
            return command
         elif command == "err1":
            print("Error: Invalid email")
            return
         elif command == "err2":
            print("Error: Email and password didn't match or don't exist")
            return
   

   def account_creation_client():
      """Allows the user to create an account with an email and password, also adds a username to be his display name"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = createaccount_pb2_grpc.CreateAccountStub(channel)
         # Takes in email, password and username as input, then creates the account
         get_account_details = stub.create_account(createaccount_pb2.AccountInfoInput(email=str(input("Please enter your email: ")), password = str(input("Please enter your password: ")), username = str(input("Please enter your desired username: ")))) 
         output = get_account_details.message.split(" ")[0]
         email = get_account_details.message.split(" ")[1]
         password = get_account_details.message.split(" ")[2]
         username = get_account_details.message.split(" ")[3]
         if output == "err1":
            print("Error: Invalid email")
         elif output == "err2":
            print("Error Invalid password")
         elif output == "err3":
            print("Error: Invalid username")
         elif output == "err4":
            print("Error: Email already exists")
         elif output == "c":
            print("Account Created - Email:",email, "Password:",password, "Username:",username)
         return
      

   def main_client(user, selection = "start"):
      """Gives the user the main menu of the application"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = login_pb2_grpc.LoginStub(channel)
         stub2 = destination_pb2_grpc.GetDestinationStub(channel)
         choice = "1"
         get_destinations = stub2.get_destination(destination_pb2.DestinationInput(getdest=choice))
         destinations = get_destinations.message
         #Again, we use strings to go between server and client for ease of use, so here we fix the string into a list
         dests = destinations.split("!")
         count = 0
         for dest in dests:
            destin = dest.split(",")
            dests[count] = destin
            count += 1
         while selection != "k":
            print("\n"*3,f"---- {user} ----")
            print("\n"*3,"Options\n1. Log out\n2. Edit account details\n3. Get destinations\n4. My reviews\n5. View Favorites\n6. View Calendar\n7. Wipe Calendar\nk. Kill\n")
            selection = str(input("Select: ")).lower()
            if selection != "1" and selection != "2" and selection != "3" and selection != "4" and selection != "5" and selection != "6" and selection != "7" and selection != "k":
               print("Error: Invalid input")
            else:
               if selection == "1":
                  StartupInterface.logout()  # Logs the user out
                  return
               elif selection == "2":
                  selection = Account_client.account_details_client(user) # Goes to a client where account information can be edited
                  if selection == "k":
                     return "k"
               elif selection == "3":
                  selection = Destination_client.get_destinations_client(user) # Goes to a client to see all destinations or filter destinations
               elif selection == "4":
                  selection = Account_client.user_reviews_client(user, dests) # Goes to a client where the user can see all his reviews and make changes or delete them
               elif selection == "5":
                  selection = Account_client.view_favorites_client(user) # Goes to a client where the user can see things he has favorited
               elif selection == "6":
                  selection = Calendar_client.view_calendar_client_start(user) #Goes to a client where the user can see a calendar with his plans
               elif selection == "7":
                  selection = Calendar_client.wipe_calendar_client(user) #Goes to a client where the user can wipe his entire calendar
               elif selection == "k":
                  '''kill client'''
                  return "k"
            if selection == "k":
               return "k"
            

class Account_client():
   def account_details_client(user, selection = "start"):
      """Gives options to edit the users account information"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = changepass_pb2_grpc.ChangePassStub(channel)
         while selection != "k":
            print(f"---- {user} ----\n")
            print("\n"*3,"Options\n1. Change username\n2. Change password\n3. Delete account\nb. Back\nk. Kill\n")
            selection = str(input("Select: ")).lower()
            if selection != "1" and selection != "2" and selection != "3" and selection != "b" and selection != "k":
               print("Error: Invalid input")
            else:
               if selection == "1":
                  selection = Account_client.change_username_client(user)
               elif selection == "2":
                  selection = Account_client.change_pass_client(user)
               elif selection == "3":
                  selection = Account_client.delete_account_client(user)
                  if selection == "k":
                     return "k"
               elif selection == "b":
                  '''go back'''
                  return
               elif selection == "k":
                  '''kill client'''
                  return "k"
               
   def delete_account_client(user):
      """Give the user the option to delete his account, must enter password twice to confirm"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = createaccount_pb2_grpc.CreateAccountStub(channel)
         response = stub.delete_account(createaccount_pb2.DeletionInput(password = str(input("Please enter your current password for your security: ")), confirm_password = str(input("Please enter re-enter your password for confirmation: ")), email = user))
         command = response.message.split(" ")[0]
         if command == "k":
            print("Account deleted")
            return "k"
         elif command == "err1":
            print("Error: Incorrect password")
            return
         elif command == "err2":
            print("Error: Passwords did not match")
            return

               

   def change_username_client(user):
      """Client that lets the user change his username"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = changepass_pb2_grpc.ChangePassStub(channel)
         stub2 = changeusername_pb2_grpc.ChangeNameStub(channel)
         # Needs password confirmation to change the username of the current user
         security_check = stub.check_current_pass(changepass_pb2.CurrPassInput(currpass = str(input("Please enter your current password for your security: ")), email = user))
         result = security_check.message.split(" ")[0]
         if result == "True":
            get_new_username = stub2.change_name(changeusername_pb2.NewNameInput(newname = str(input("Please enter your new username: "))))
            new_username = get_new_username.message.split(" ")[0]
            if new_username == "False":
               print("Error: Invalid username")
               return
            else:
               get_confirmation = stub2.confirm_name(changeusername_pb2.ConfirmNameInput(confname = str(input("Please re-enter your new username for confirmation: ")), nametocompare = new_username, email = user ))
               confirmation = get_confirmation.message.split(" ")[0]
               if confirmation == "True":
                  print("Username has been changed")
                  return
               else:
                  print("Error: Usernames didn't match")
                  return
         else:
            print("Error: Incorrect password")
            return

   def change_pass_client(user):
      """Client that lets the user change his password"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = changepass_pb2_grpc.ChangePassStub(channel)
         '''Needs password confirmation to change the username of the current user'''
         security_check = stub.check_current_pass(changepass_pb2.CurrPassInput(currpass = str(input("Please enter your current password for your security: ")), email = user))
         result = security_check.message.split(" ")[0]
            # User must enter the new password twice then the input is compared to complete the password change
         if result == "True":
            get_new_password = stub.change_pass(changepass_pb2.NewPassInput(newpass = str(input("Please enter your new password: "))))
            new_password = get_new_password.message.split(" ")[0]
            if new_password == "False":
               print("Error: Invalid password")
               return
            else:
               get_confirmation = stub.confirm_current_pass(changepass_pb2.ConfirmPassInput(confirmpass = str(input("Please re-enter your new password for confirmation: ")), passtocompare = new_password, user = user))
               confirmation = get_confirmation.message.split(" ")[0]
               if confirmation == "True":
                  print("Password has been changed")
                  return
               else:
                  print("Error: Passwords didn't match")
                  return
         else:
            print("Error: Incorrect password")
            return
         
   
   def view_favorites_client(user):
      """Client to view favorite destinations"""
      with grpc.insecure_channel('localhost:50051') as channel:
         #Get first stub for favorite destinations
         stub3 = favoritedestination_pb2_grpc.FavoriteDestinationStub(channel)
         #The user's email is given to the server and the server returns the users favorite destinations
         favoritedests = stub3.getfavorites(favoritedestination_pb2.GetFavoriteInput(email=user))
         #The first fix is getting just the ids of the destinations
         favoritedestsfix1 = favoritedests.destid
         #If this string is empty, we tell the user he has no favorites
         if favoritedestsfix1 == "-":
            print("You have no favorites")
            pause = input("Enter anything to continue: ")
            return
         #Otherwise we move onto fix 2
         else:
            #Where we split the string up by a dash
            favoritedestsfix2 = favoritedestsfix1.split("-")
            #Then we open the weather channel and get the stub
            with grpc.secure_channel('hugb-service-ep3thvejnq-uc.a.run.app:443', grpc.ssl_channel_credentials()) as channel:
               stub2 = weatherStation_pb2_grpc.WeatherStationStub(channel)
               #And get the destinations channel and stub
               with grpc.insecure_channel('localhost:50051') as channel:
                  stub = destination_pb2_grpc.GetDestinationStub(channel)
                  #Just decide to sort the users favorites by city instead of making him go through an extra menu for what will probably be a short list
                  get_destinations = stub.get_destination(destination_pb2.DestinationInput(getdest="1"))
                  #Get the message
                  destinations = get_destinations.message
                  #Split the destinations up (they are seperated by ! in the string)
                  dests = destinations.split("!")
                  #Start a count
                  count = 0
                  #Go over each destination in this new list that was split up
                  for dest in dests:
                     #Split up the destination into a list for ease of use
                     destin = dest.split(",")
                     #Say that list[count] = fixed list
                     dests[count] = destin
                     #Add 1 to count
                     count += 1
                  #Start a new menu to print the favorites
                  selection = "start"
                  while selection != "k":
                     print("\n"*3)
                     header = '{:<4} {:<21} {:<21} {:<7} {:<16}'.format("ID", "City", "Country", "Rating", "Coordinates")
                     print(header)
                     count = 1
                     #Create a new list over favorite destinations
                     favoritedestslist = []
                     #Format each destination and print it with a count to act as ID
                     for dest in dests:
                        #If the id in our destinations list matches one of the ids in the users favorites, we print it
                        if dest[4] in favoritedestsfix2:
                           #Gets the current weather for the coordinates for the current city from the weatherstation server
                           current_weather = stub2.get_current_weather(weatherStation_pb2.Location(lat=float(dest[2].split('/')[0]), lon=float(dest[2].split('/')[1]), api_key='0e2a749a-c6fb-4a3d-b7e3-64c039935659'))
                           #Formats the information about the destination to look good
                           printdest = '{:<4} {:<21} {:<21} {:<7} {:<16}'.format(count, dest[0], dest[1], dest[3], dest[2])

                           #Prints first the information about the destination and next the information about the current weather
                           print(printdest+'\n'*2+'Current Weather in City:\n'+str(current_weather))
                           #Once printed we add the destination to our favorite list
                           favoritedestslist.append(dest)
                           #Add to the count (Count here is used for nicer looking ids which are totally seperate from actual ids)
                           count += 1
                     print("b. Back")
                     print("k. Kill")
                     #Ask for users input
                     selection = input("Select: ").lower()
                     #Try turning his selection into a number
                     try:
                        intselection = int(selection)
                        #If that goes through, if the number is larger than 0 and less than or equal to the length of the list
                        if intselection > 0 and intselection <= len(dests):
                           #We get the index by doing the selection -1
                           index = intselection-1
                           #Then start the new client where he can see the destination better
                           selection = Account_client.view_favorite_client(user, index, favoritedestslist)
                     #If the int(x) fails, we just pass as he could've selected back or kill
                     except:
                        pass
                     if selection == "b":
                        return
                     elif selection == "k":
                        return "k"
                     else:
                        print("Error: Invalid Input")
                  if selection == "k":
                     return "k"
      

   def view_favorite_client(user, index, dests):
      """Client to view a singular favorite destination"""
      with grpc.insecure_channel('localhost:50051') as channel:
         #Get favorite destination stub
         stub = favoritedestination_pb2_grpc.FavoriteDestinationStub(channel)
         #Start a selection menu based on the destination
         selection = "start"
         while selection != "k":
            #Print the destination name
            print("--"+dests[index][0]+"--")
            print("1. Remove from favorites")
            print("2. View Events")
            print("3. View Entertainments")
            print("b. Back")
            print("k. Kill")
            selection = input("Select: ").lower()
            if selection == "1":
               #If he selects 1, he wants to remove it from favorites, we get the id and change it to a string
               id = str(dests[index][4])
               #Print the response telling him that it has been removed (We input his email and the id)
               print(stub.deletefavortie(favoritedestination_pb2.DeleteFavoriteInput(email=str(user), favoritedest=id)))
               #Give him a pause to let him process things
               pause = input("Enter anything to continue: ")
               #Go back as the location is no longer favorited
               return "b"
            elif selection == "2":
               #Start events with the destination id
               selection = Events_client.StartEvents("start",dests[index][4])
            elif selection == "3":
               #Start entertainments with the destination id
               selection = Entertainment_client.StartEntertainments("start",dests[index][4])
            elif selection == "b":
               return
            elif selection == "k":
               return "k"
            else:
               print("Error: Invalid Input")
   
   
   def user_reviews_client(user, dests, selection = "start"):
      """Allows the user to view his past reviews and edit or deleted the already existing reviews"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = destination_pb2_grpc.GetDestinationStub(channel)
         while selection != "k":
            get_user_reviews = stub.get_user_reviews(destination_pb2.UserReviewsInput(email=str(user)))
            user_reviews = get_user_reviews.message.split("!-!_!")
            if user_reviews==['']: # If user has no reviews
               print(f"\n\n\n----{user}----\n\n# Reviews #\n\n") # Prints active user
               print("Whoops! looks like you don't have any existing reviews.\n\n")
            else:
               count = 0
               print(f"\n\n\n----{user}----\n\n# Reviews #\n\n") # Prints active user
               header = "{:<4} {:<18} {}\n".format("ID", "City", "Rating")
               print(header)
               for review in user_reviews:  
                  review = review+","+str(count+1) # adds temporary id
                  review_list = review.split(",") # converts the elements into lists
                  user_reviews[count] = review_list # replacing the string version of info with list versions of info
                  count += 1
               for review in user_reviews:
                  dest_id = int(review[1]) # Destination_id is stored at review[1]
                  for dest in dests:
                     if str(dest_id) == dest[4]: # Match the id of the review with the destination in the destinations list
                        destination = "--" + dest[0] + "--" # Destination name is stored at dest[0]
                        review[2] = str(review[2]).replace(";",",") # "," in stored reviews are replaced with ";" because we are seperating elements in csv with "," so we need to change them back
                        print(f"{review[5]}. {destination:<20} ★ {review[4]} ★\n{review[2]}\n\n") # {Printing the temporary id} 1 to n, {the destination corresponding to the review}, {the review of the destination}

            print("\n"*3,'Options\n1. Edit review\n2. Delete review\nb. Back\nk. Kill\n')
            selection = str(input("Select: ")).lower()
            if selection == "1":
               review_to_edit = str(input("Enter the ID of the review you want to edit: ")).lower()
               for review in user_reviews:
                  if review_to_edit == str(review[5]): # Finding the temporary id that matches the ID that was entered
                     for dest in dests:
                        if str(review[1]) == dest[4]: # review[1] = destination_id in "review", dest[4] = destination_id in "dest" if they match we can get the-
                           destination = dest[0] # Destination name
                           dest_id = review[1] # Not really needed we just want to declared things before hand when using gRPC since it's sensitive to different inputs
                           edited_review = str(input(f"\nNew review for {destination}: "))
                           edit_rating_option = str(input(f"\nWould you like to change your rating for {destination}, (current rating: ★ {review[4]} ★)? \n1. Yes\n2. No\n\nSelection: "))
                           if edit_rating_option == "1":
                              rating = str(input(f"\nRating for {destination}(1-5): "))
                              while rating != "1" and rating != "2" and rating != "3" and rating != "4" and rating != "5":
                                 print("\nInvalid rating! (Must be an integer between 1-5)\n")
                                 rating = str(input(f"\nRating for {destination}: "))
                           else:
                              rating = "null"
                           edit_review = stub.edit_dest_review(destination_pb2.ReviewEditInput(email=str(user), dest_id=str(dest_id), new_review = str(edited_review), rating = str(rating))) # Goes into our logic and edits the review and returns an output message from the server
               output_message2 = edit_review.message.split(" ")[0]
               if output_message2 == "y":
                  print("Review edited!")
               else:
                  print("Error: Review edit went wrong")
            elif selection == "2":
               review_to_edit = str(input("Enter the ID of the review you want to delete: ")).lower()
               for review in user_reviews:
                  if review_to_edit == str(review[5]): # Finding the temporary id that matches the ID that was entered
                     for dest in dests:
                        if str(review[1]) == dest[4]: # review[1] = destination_id in "review", dest[4] = destination_id in "dest" if they match we can get the-
                           destination = dest[0] # Destination name
                           dest_id = review[1] # Not really needed we just want to declared things before hand when using gRPC since it's sensitive to different inputs
               delete_review = stub.delete_user_review(destination_pb2.DelReviewInput(email=str(user), dest_id=str(dest_id)))
               if delete_review.message.split(" ")[0] == "del":
                  print("Review deleted successfully!")
               else:
                  print("Deletion failed")
            elif selection == "b":
               return
            elif selection == "k":
               return "k"
            else:
               print("Error: Invalid Input")


class Destination_client():
   def get_destinations_client(user):
      """Client to get destinations filter choice"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = destination_pb2_grpc.GetDestinationStub(channel)
         #Print users options for filtering
         print("\n"*3,"Options\n1. Sort by City\n2. Sort by Country\n3. Search for City\n4. Sort by Rating\n5. Sort by Popularity\nb. Back\nk. Kill\n")
         selection = str(input("Select: ")).lower()
         #Make sure user selects correct options, otherwise warn him
         if selection != "1" and selection != "2" and selection != "3" and selection != "4" and selection != '5' and selection != "b" and selection != "k":
            print("Error: Invalid input")
         else:
            #If input is 1-4, start the get_destinations_client_choice function to get the destinations
            if selection == "1" or selection == "2" or selection == "3" or selection == "4" or selection == '5':
               selection = Destination_client.get_destinations_client_choice(user, selection)
            elif selection == "b":
               return
            elif selection == "k":
               return "k"
         if selection == "k":
            return "k"


   def get_destinations_client_choice(user, choice):
      '''Client to get destinations'''
      with grpc.secure_channel('hugb-service-ep3thvejnq-uc.a.run.app:443', grpc.ssl_channel_credentials()) as channel:
         #Connecting to the weatherstation server making sure to name the stub something different from the stub for the other server, in this case it is simply stub2
         stub2 = weatherStation_pb2_grpc.WeatherStationStub(channel)
         with grpc.insecure_channel('localhost:50051') as channel:
            stub = destination_pb2_grpc.GetDestinationStub(channel)
            #If he chose 3, he has asked to search for some city
            if choice == "3":
               #He inputs his search
               citySearch = input("Enter your search (City): ")
               #We aren't good with proto so we just use a string, so we add to the choice a - and the search
               #So it would look something like this 3-Amsterdam, this is later used in the interface
               choice = choice+"-"+citySearch
               #Get the destinations
               get_destinations = stub.get_destination(destination_pb2.DestinationInput(getdest=choice))
               destinations = get_destinations.message
            else:
               #If he hasn't searched, just get destinations
               get_destinations = stub.get_destination(destination_pb2.DestinationInput(getdest=choice))
               destinations = get_destinations.message
            #Again, we use strings to go between server and client for ease of use, so here we fix the string into a list
            dests = destinations.split("!")
            count = 0
            for dest in dests:
               destin = dest.split(",")
               dests[count] = destin
               count += 1
            #Format a header to make the menu look a bit better and print it
            selection = "start"
            while selection != "k":
               print("\n"*3)
               header = '{:<4} {:<21} {:<21} {:<7} {:<16}'.format("ID", "City", "Country", "Rating", "Coordinates")
               if choice == '5':
                  header = '{:<4} {:<21} {:<21} {:<20} {:<12} {}'.format("ID", "City", "Country", "Number of Reviews", "Rating", "Coordinates")
               print(header)
               count = 1
               #Format each destination and print it with a count to act as ID
               for dest in dests:
                  #Gets the current weather for the coordinates for the current city from the weatherstation server
                  current_weather = stub2.get_current_weather(weatherStation_pb2.Location(lat=float(dest[2].split('/')[0]), lon=float(dest[2].split('/')[1]), api_key='0e2a749a-c6fb-4a3d-b7e3-64c039935659'))
                  #Formats the information about the destination to look good
                  printdest = '{:<4} {:<21} {:<21} {:<7} {:<16}'.format(count, dest[0], dest[1], dest[3], dest[2])
                  if choice == '5':
                     printdest = printdest = '{:<4} {:<21} {:<21} {:<20} {:<12} {}'.format(count, dest[0], dest[1],dest[5], dest[3], dest[2])

                  #Prints first the information about the destination and next the information about the current weather
                  print(printdest+'\n'*2+'Current Weather in City:\n'+str(current_weather))
                  count += 1
               print("\n"*3,"Options\n1. Enter destination ID for more options\nb. Back\nk. Kill\n")
               selection = input("Select: ").lower()
               if selection == "1":
                  dest_id = input("\nEnter destination ID: ").lower()
                  try:
                     intdest_id = int(dest_id)
                     if intdest_id > 0 and intdest_id <= len(dests):
                        index = intdest_id-1
                        selection = Destination_client.view_destination_client(user, index, dests)
                  except:
                     print("Error: ID does not exist")
                     pass
               elif selection == "b":
                  return
               elif selection == "k":
                  return "k"
               elif selection == "1":
                  pass
               else:
                  print(selection)
                  print("Error: Invalid Input")
            if selection == "k":
               return "k"
   
   def view_destination_client(user, dest, dests, selection="start"):
      """Gives a menu with option for a specific destination"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = favoritedestination_pb2_grpc.FavoriteDestinationStub(channel)
         while selection != "k":
            print(f"\n---- {dests[dest][0]} ----")
            print("\n"*3,"Options\n1. Add to favorites\n2. Leave a review\n3. View Events\n4. View Entertainments\n5. View Destination Reviews\nb. Back\nk. Kill\n")
            selection = input("Select: ").lower()
            dest_id = str(dests[dest][4])
            if selection == "1":
               print(stub.newfavorite(favoritedestination_pb2.FavoriteInput(email=str(user), favoritedest=dest_id)).favoriteconfirmation)
               pause = input("Enter anything to continue: ")
            elif selection == "2":
               selection = Reviews_client.add_review_client(user, dest_id, dest, dests) # Client to add a review and rating to the destination
            elif selection == "3":
               selection = Events_client.StartEvents(user, 'start', dest_id) # Client to view events for the destination
            elif selection == "4":
               selection = Entertainment_client.StartEntertainments(user, 'start', dest_id) # CLient to view entertainments for the destination
            elif selection == "5":
               selection = Reviews_client.destination_reviews_client(dest_id,dests) # Client to see all reviews made on the chosen destination
            elif selection == "b":
               return
            elif selection == "k":
               return "k"
            else:
               print("2Error: Invalid Input")
            if selection == "k":
               return "k"



class Events_client():
   def StartEvents(user, selection="start",destID='0'):
      """This Function Enables The User To Call Other Functions That Filter The Events"""
      while selection != "k":
         print("\n"*3,'Options\n1. All Events\n2. Events in Price Range: \n3. Events Equal To X In Price: \n4. Filter by Accessibility\nb. Back\nk. Kill\n')
         selection = str(input("Select: ")).lower()
         if selection == '1':
            selection = Events_client.AllEvents(user, 'start',destID)
         elif selection == '2':
            selection = Events_client.EBetween(user, 'start',destID)
         elif selection == '3':
            selection = Events_client.EEqual(user, 'start',destID)
         elif selection == '4':
            selection = Events_client.EAccess(user, 'start',destID)
         elif selection == 'b':
            return
         elif selection == "k":
            return "k"
         else:
            print('Wrong Input')
         if selection == "k":
            return "k"

   def AllEvents(user, selection="start", destID='0'):
      """Prints the information for all of the Events"""
      with grpc.insecure_channel('localhost:50051', options=(('grpc.enable_http_proxy', 0),)) as channel:
         #Create a stub to be able to call the proto functions
         #Prints the desired message from the app interface
         stub = events_pb2_grpc.GetEventsStub(channel)
         x = stub.get_events(events_pb2.ClientInput(eventinput='1-'+destID)).events
         list = x.split("-")
         count = 0
         idlist = []
         for event in list:
            if len(event) < 10:
               idlist.append(event)
               list.remove(event)
         idlist.pop(0)
         print("ID "+list[0])
         while selection != "k":
            #Lets the user choose how they proceed
            count = 0
            for x in list:
               try:
                  print(str(count+1)+"  "+list[count+1])
                  count += 1
               except:
                  break
            print("\n"*3,'Options\nPlan. (Int of plan)\nb. Back\nk. Kill\n')
            selection = str(input("Select: ")).lower()
            if Validators.IntCheck(selection):
               if int(selection) >= 1 and int(selection) <= len(list)-1:
                  selection = Calendar_client.event_planning_client(list[int(selection)], user, destID, idlist[int(selection)-1])
                  return selection
                  #Muna
               else:
                  print("Not in range")
            elif selection == 'b':
               return destID
            elif selection == 'k':
               return "k"
            else:
               print('Wrong Input')


   def EBetween(user, selection="start",destID='0'):
      """Prints information for every event between a certain price range"""
      with grpc.insecure_channel('localhost:50051', options=(('grpc.enable_http_proxy', 0),)) as channel:
         stub = events_pb2_grpc.GetEventsStub(channel)
         #Create a stub to be able to call the proto functions
         #Asks the user for a minimum and maximum price
         minprice = input('\nPlease Enter a Minimum Price: ')
         maxprice = input('\nPlease Enter a Maximum Price: ')
         #Checks if the chosen prices are integers
         if Validators.IntCheck(minprice) and Validators.IntCheck(maxprice):
            x = stub.get_events(events_pb2.ClientInput(eventinput='2-'+str(minprice)+"-"+str(maxprice)+"-"+destID)).events
            list = x.split("-")
            print(list)
            count = 0
            idlist = []
            for event in list:
               if len(event) < 10:
                  idlist.append(event)
                  list.remove(event)
            idlist.pop(0)
         else:
            print("Invalid Input")
            return destID
         while selection != "k":
            print("ID "+list[0])
            count = 0
            for x in list:
               try:
                  print(str(count+1)+"  "+list[count+1])
                  count += 1
               except:
                  break
            #Lets the user choose how they proceed
            print("\n"*3,'Options\nPlan. (Int of plan)\nb. Back\nk. Kill\n')
            selection = str(input("Select: ")).lower()
            if Validators.IntCheck(selection):
               if int(selection) >= 1 and int(selection) <= len(list)-1:
                  selection = Calendar_client.event_planning_client(list[int(selection)], user, destID, idlist[int(selection)-1])
                  return selection
                  #Muna
               else:
                  print("Not in range")
            elif selection == "b":
                  return destID
            elif selection == "k":
                  return "k"
            else:
                  print('Wrong Input')



   def EEqual(user, selection="start",destID='0'):
      """Prints the information for every event where the price is equal to an input from the user"""
      with grpc.insecure_channel('localhost:50051', options=(('grpc.enable_http_proxy', 0),)) as channel:
         #Create a stub to be able to call the proto functions
         stub = events_pb2_grpc.GetEventsStub(channel)
         #Asks the user for a price
         Price = input('\nPlease Enter a Price: ')
         #Checks if the price chosen by the user is an integer
         if Validators.IntCheck(Price):
            #Gets the correct information and prints it
            x = stub.get_events(events_pb2.ClientInput(eventinput='3-'+str(Price)+"-"+destID)).events
            list = x.split("-")
            count = 0
            idlist = []
            for event in list:
               if len(event) < 10:
                  idlist.append(event)
                  list.remove(event)
            idlist.pop(0)
         else:
            print('Invalid Input')
            return
         while selection != "k":
            print("ID "+list[0])
            count = 0
            for x in list:
               try:
                  print(str(count+1)+"  "+list[count+1])
                  count += 1
               except:
                  break
            print("\n"*3,'Options\nPlan. (Int of plan)\nb. Back\nk. Kill\n')
            selection = str(input("Select: ")).lower()
            if Validators.IntCheck(selection):
               if int(selection) >= 1 and int(selection) <= len(list)-1:
                  selection = Calendar_client.event_planning_client(list[int(selection)], user, destID, idlist[int(selection)-1])
                  return selection
                  #Muna
               else:
                  print("Not in range")
            elif selection == "b":
                  return destID
            elif selection == "k":
                  return "k"
            else:
                  print('Wrong Input')

   def EAccess(user, selection="start",destID='0'):
      """Prints the information for every event where the chose accessibility is available for a select destination"""
      with grpc.insecure_channel('localhost:50051', options=(('grpc.enable_http_proxy', 0),)) as channel:
         #Create a stub to be able to call the proto functions
         stub = events_pb2_grpc.GetEventsStub(channel)
         #Asks the user for what they would like to filter by
         acc = str(input('1. Wheelchair Accessible\n2. Kid Friendly\n3. Alcohol Available\n\nSelection: '))
         if acc == '1':
            #Gets the correct information and prints it
            chosenacc = 'Wheelchair Accessible'
            x = stub.get_events(events_pb2.ClientInput(eventinput='4-'+str(chosenacc)+'-'+destID)).events
         elif acc == '2':
            #Gets the correct information and prints it
            chosenacc = 'Kid Friendly'
            x = stub.get_events(events_pb2.ClientInput(eventinput='4-'+str(chosenacc)+'-'+destID)).events
         elif acc == '3':
            #Gets the correct information and prints it
            chosenacc = 'Alcohol Available'
            x = stub.get_events(events_pb2.ClientInput(eventinput='4-'+str(chosenacc)+'-'+destID)).events
         else:
            print('Invalid Input')
            return
         list = x.split("-")
         count = 0
         idlist = []
         for event in list:
            if len(event) < 10:
               idlist.append(event)
               list.remove(event)
         idlist.pop(0)
         while selection != "k":
            print("ID "+list[0])
            count = 0
            for x in list:
               try:
                  print(str(count+1)+"  "+list[count+1])
                  count += 1
               except:
                  break
            print("\n"*3,'Options\nPlan. (Int of plan)\nb. Back\nk. Kill\n')
            selection = str(input("Select: ")).lower()
            if Validators.IntCheck(selection):
               if int(selection) >= 1 and int(selection) <= len(list)-1:
                  selection = Calendar_client.event_planning_client(list[int(selection)], user, destID, idlist[int(selection)-1])
                  return selection
               else:
                  print("Not in range")
            elif selection == 'b':
                  return destID
            elif selection == 'k':
                  return "k"
            else:
                  print('Wrong Input')


class Reviews_client():
   def add_review_client(user, dest_id, dest, dests, selection="start"):
      """Allows the user to add a review and rating to the destination"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = destination_pb2_grpc.GetDestinationStub(channel)
         review = str(input(f"\nReview for {dests[dest][0]}: "))
         rating = str(input(f"\nRating for {dests[dest][0]}(1-5): "))
         while rating != "1" and rating != "2" and rating != "3" and rating != "4" and rating != "5":
            print("\nInvalid rating! (Must be an integer between 1-5)\n")
            rating = str(input(f"\nRating for {dests[dest][0]}: "))
         make_review = stub.get_dest_review(destination_pb2.ReviewInput(email=str(user), dest_id=str(dest_id), review = str(review), rating=str(rating)))
         output_message = make_review.message.split(" ")[0]
         if output_message == "y": # If there was no preexisting review
            print("Review complete!")
            return
         elif output_message == "n": # If there was a preexisting review
            print("Oops! It looks like you already reviewed this location, would you like to replace the already existing review?")
            while selection != "k":
               edit_selection = str(input("(y/n): "))
               if edit_selection == "y":
                  edit_review = stub.edit_dest_review(destination_pb2.ReviewEditInput(email=str(user), dest_id=str(dest_id), new_review = str(review), rating=str(rating)))
                  output_message2 = edit_review.message.split(" ")[0]
                  if output_message2 == "y":
                     print("Review edited!")
                     return "back"
                  else:
                     print("Error: Review edit went wrong")
               elif edit_selection == "n":
                  print("Review left untouched.")
                  return "back"
               else:
                  print("Invalid Input")
         else:
            return "back"


   def destination_reviews_client(dest_id, dests):
      """Fetches all reviews for a certain location and prints them out"""
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = destination_pb2_grpc.GetDestinationStub(channel)
         get_dest_reviews = stub.get_reviews_on_dest(destination_pb2.GetReviewsInput(get_reviews=dest_id)) # gets reviews off destination ID
         dest_reviews = get_dest_reviews.message.split("!-!_!") # Weird string to prevent wrong seperation in each review, splitting
         count = 0                                              # on other symbols could cause problems because people can type anything.
         if dest_reviews == ['']:
            print(f"\n\n\n----{dests[0][0]}----\n\n# Reviews #\n\n")
            print("Whoops! looks like there are no existing reviews for this location.\n\n")
            return "back"
         else:
            print(f"\n\n\n----{dests[0][0]}----\n\n# Reviews #\n\n")
            for review in dest_reviews:
               review_list = review.split(",")
               dest_reviews[count] = review_list
               count += 1

            for review in dest_reviews:
               symbols = len(review[2])
               review[2] = str(review[2]).replace(";",",") # "," in stored reviews are replaced with ";" because we are seperating elements in csv with "," so we need to change them back
               print("-"*242)
               print(f"{review[3]}:\n\n{review[2]}\n\nRating: ★ {review[4]} ★")
               print("_"*242,"\n")
            return "back"


class Entertainment_client():
   def StartEntertainments(user, selection="start",dest_id='0'):
      """This function enables the user to call other functions that filter the entertainments"""
      while selection != 'k':
         print("\n"*3,'Options\n1. All Entertainments\n2. Entertainmnets in Price Range: \n3. Entertainments Equal To X In Price: \n4. Filter by Accessibility\nb. Back\nk. Kill\n')
         selection = str(input('Select: ')).lower()
         if selection == '1':
            selection = Entertainment_client.AllEntertainments(user, "start",dest_id)
         elif selection == '2':
            selection = Entertainment_client.EntBetweenPrices(user, "start",dest_id)
         elif selection == '3':
            selection = Entertainment_client.EntEqualtoPrice(user, "start",dest_id)
         elif selection == '4':
            selection = Entertainment_client.EntAccFilter(user, 'start',dest_id)
         elif selection == 'b':
            return
         elif selection == 'k':
            return "k"
         else:
            print('Wrong Input')
         if selection == 'k':
            return 'k'

   def AllEntertainments(user, selection="start",dest_id='0'):
      """Prints the information for all of the entertainments"""
      with grpc.insecure_channel('localhost:50051', options=(('grpc.enable_http_proxy', 0),)) as channel:
         #Create a stub to be able to call the proto functions
         stub = entertainments_pb2_grpc.EntertainmentsStub(channel)
         #Prints the desired message from the app interface
         x = stub.get_entertainments(entertainments_pb2.EntertainmentInput(entmtinput='1-'+str(dest_id))).entertainments
         list = x.split("-")
         count = 0
         idlist = []
         for event in list:
            if len(event) < 10:
               idlist.append(event)
               list.remove(event)
         idlist.pop(0)
         while selection != "k":
         #Lets the user choose how they proceed
            print("ID "+list[0])
            count = 0
            list.pop(-1)
            for x in list:
               try:
                  print(str(count+1)+"  "+list[count+1])
                  count += 1
               except:
                  break
            print("\n"*3, 'Options\nPlan. (Int of plan)\nb. Back\nk. Kill\n')
            selection = str(input("Select: ")).lower()
            if Validators.IntCheck(selection):
               if int(selection) >= 1 and int(selection) <= len(list)-1:
                  selection = Calendar_client.entertainment_planning_client(list[int(selection)], user, dest_id, idlist[int(selection)-1])
                  return selection
                  #Muna
               else:
                  print("Not in range")
            elif selection == 'b':
               return dest_id
            elif selection == 'k':
               return "k"
            else:
               print('Wrong Input')


   def EntBetweenPrices(user, selection="start",dest_id='0'):
      """Prints information for every entertainment between a certain price range"""
      with grpc.insecure_channel('localhost:50051', options=(('grpc.enable_http_proxy', 0),)) as channel:
         #Create a stub to be able to call the proto functions
         stub = entertainments_pb2_grpc.EntertainmentsStub(channel)
         #Asks the user for a minimum and maximum price
         minprice = input('\nPlease Enter a Minimum Price: ')
         maxprice = input('\nPlease Enter a Maximum Price: ')
         #Checks if the chosen prices are integers
         if Validators.IntCheck(minprice) and Validators.IntCheck(maxprice):
               #Prints the correct information
               x = stub.get_entertainments(entertainments_pb2.EntertainmentInput(entmtinput='2-'+str(minprice)+'-'+str(maxprice)+'-'+str(dest_id))).entertainments
               list = x.split("-")
               count = 0
               idlist = []
               for event in list:
                  if len(event) < 10:
                     idlist.append(event)
                     list.remove(event)
               idlist.pop(0)
         else:
               print('Invalid Input')
               return dest_id
         while selection != "k":
            print("ID "+list[0])
            count = 0
            list.pop(-1)
            for x in list:
               try:
                  print(str(count+1)+"  "+list[count+1])
                  count += 1
               except:
                  break
            #Lets the user choose how they proceed
            print("\n"*3,'Options\nPlan. (Int of plan)\nb. Back\nk. Kill\n')
            selection = str(input("Select: ")).lower()
            if Validators.IntCheck(selection):
               if int(selection) >= 1 and int(selection) <= len(list)-1:
                  selection = Calendar_client.entertainment_planning_client(list[int(selection)], user, dest_id, idlist[int(selection)-1])
                  return selection
                  #Muna
               else:
                  print("Not in range")
            elif selection == 'b':
               return dest_id
            elif selection == 'k':
               return "k"
            else:
               print('Wrong Input')


   def EntEqualtoPrice(user, selection="start",dest_id='0'):
      """Prints the information for every event where the price is equal to an input from the user"""
      with grpc.insecure_channel('localhost:50051', options=(('grpc.enable_http_proxy', 0),)) as channel:
         #Create a stub to be able to call the proto functions
         stub = entertainments_pb2_grpc.EntertainmentsStub(channel)
         #Asks the user for a price
         Price = input('\nPlease Enter a Price: ')
         #Checks if the price chosen by the user is an integer
         if Validators.IntCheck(Price):
            #Gets the correct information and prints it
            x = stub.get_entertainments(entertainments_pb2.EntertainmentInput(entmtinput='3-'+str(Price)+'-'+str(dest_id))).entertainments
            list = x.split("-")
            count = 0
            idlist = []
            for event in list:
               if len(event) < 10:
                  idlist.append(event)
                  list.remove(event)
            idlist.pop(0)
         else:
            print("Ivalid Input")
            return
         while selection != "k":
            print("ID "+list[0])
            count = 0
            list.pop(-1)
            for x in list:
               try:
                  print(str(count+1)+"  "+list[count+1])
                  count += 1
               except:
                  break
            print("\n"*3,'Options\nPlan. (Int of plan)\nb. Back\nk. Kill\n')
            selection = str(input("Select: ")).lower()
            if Validators.IntCheck(selection):
               if int(selection) >= 1 and int(selection) <= len(list)-1:
                  selection = Calendar_client.entertainment_planning_client(list[int(selection)], user, dest_id, idlist[int(selection)-1])
                  return selection
                  #Muna
               else:
                  print("Not in range")
            elif selection == 'b':
               return dest_id
            elif selection == 'k':
               return "k"
            else:
               print('Wrong Input')
   
   def EntAccFilter(user, selection="start",destID='0'):
      """Prints the information for every event where the chose accessibility is available for a select destination"""
      with grpc.insecure_channel('localhost:50051', options=(('grpc.enable_http_proxy', 0),)) as channel:
         #Create a stub to be able to call the proto functions
         stub = entertainments_pb2_grpc.EntertainmentsStub(channel)
         #Asks the user for what they would like to filter by
         acc = str(input('1. Wheelchair Accessible\n2. Kid Friendly\n3. Alcohol Available\n\nSelection: '))
         if acc == '1':
            #Gets the correct information and prints it
            chosenacc = 'Wheelchair Accessible'
            x = stub.get_entertainments(entertainments_pb2.EntertainmentInput(entmtinput='4-'+chosenacc+'-'+destID)).entertainments
         elif acc == '2':
            #Gets the correct information and prints it
            chosenacc = 'Kid Friendly'
            x = stub.get_entertainments(entertainments_pb2.EntertainmentInput(entmtinput='4-'+chosenacc+'-'+destID)).entertainments
         elif acc == '3':
            #Gets the correct information and prints it
            chosenacc = 'Alcohol Available'
            x = stub.get_entertainments(entertainments_pb2.EntertainmentInput(entmtinput='4-'+chosenacc+'-'+destID)).entertainments
         else:
            print('Invalid Input')
            return
         list = x.split("-")
         count = 0
         idlist = []
         for event in list:
            if len(event) < 10:
               idlist.append(event)
               list.remove(event)
         idlist.pop(0)
         while selection != "k":
            print("ID "+list[0])
            count = 0
            list.pop(-1)
            for x in list:
               try:
                  print(str(count+1)+"  "+list[count+1])
                  count += 1
               except:
                  break
            print("\n"*3,'Options\nPlan. (Int of plan)\nb. Back\nk. Kill\n')
            selection = str(input("Select: ")).lower()
            if Validators.IntCheck(selection):
               if int(selection) >= 1 and int(selection) <= len(list)-1:
                  selection = Calendar_client.entertainment_planning_client(list[int(selection)], user, destID, idlist[int(selection)-1])
                  return selection
               else:
                  print("Not in range")
            elif selection == 'b':
               return destID
            elif selection == 'k':
               return "k"
            else:
               print('Wrong Input')


class Calendar_client():
   '''Calendar Client Class'''
   def view_calendar_client_start(user):
      '''Client to start view calendar'''
      today = datetime.date.today()
      selection = Calendar_client.view_calendar_client(today, user)
      if selection == "k":
         return "k"
      else:
         return
      

   def view_calendar_client(startdate, user):
      '''Client to show a users calendar'''
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = calendar_pb2_grpc.CalendarStub(channel)
         selection = "start"
         dateyear = startdate.year
         datemonth = startdate.month
         monthrange = calendar.monthrange(dateyear, datemonth)
         maxdays = monthrange[1]
         while selection != "k":
            output = stub.print_month(calendar_pb2.DateInput(year=str(dateyear), month=str(datemonth), user_id=user))
            print(output.month_output)
            print("\n","Options\nDay. Select a day (Integer)\nn. Next Month\np. Previous Month\nb. Back\nk. Kill")
            selection = input("Make your selection: ").lower()
            if Validators.IntCheck(selection):
               if int(selection) >= 1 and int(selection) <= maxdays:
                  selection = Calendar_client.view_date_client(dateyear, datemonth, int(selection), user)
            elif selection == "n":
               if datemonth == 12:
                  datemonth = 1
                  dateyear += 1
               else:
                  datemonth += 1
            elif selection == "p":
               if datemonth == 1:
                  datemonth = 12
                  dateyear -= 1
               else:
                  datemonth -= 1
            elif selection == "b":
               return
            elif selection == "k":
               return "k"
            else:
               print("Invalid Input")
         if selection == "k":
            return "k"
         
         
   def view_date_client(year, month, day, user):
      '''Client to view plans on a certain date'''
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = calendar_pb2_grpc.CalendarStub(channel)
         selection = "start"
         monthname = calendar.month_name[month]
         output = stub.get_date_details(calendar_pb2.DateDetailsInput(date=str(day), year=str(year), month=str(month), user_id=user))
         date_details = output.date_details
         date_details_split = date_details.split("_!##_")
         ids = date_details_split[1]
         formattedstring = date_details_split[0]
         list = formattedstring.split("\n")
         while selection != "k":
            print("\n\n--- {} {} {} ---".format(monthname, day, year)+"\n")
            if len(list) == 1:
               print("No activities for this day.")
               return
            print("ID "+list[0])
            count = 0
            for plan in list:
               if count == 0:
                  count += 1
               else:
                  print(str(count)+"  "+plan)
            print("\n"*3,"Options\nPlan. (Int of plan)\nb. Back\nk. Kill\n")
            selection = input("Select: ").lower()
            if Validators.IntCheck(selection):
               if int(selection) >= 1 and int(selection) <= len(list)-1:
                  selection = Calendar_client.plan_client(list[int(selection)], user, str(year)+"-"+str(month)+"-"+str(day))
            elif selection == "b":
               return
            elif selection == "k":
               return "k"
            else:
               print("Invalid Input")
               
               
   def event_planning_client(event, user1, destid, eid):
      '''Client to plan events'''
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = calendar_pb2_grpc.CalendarStub(channel)
         #Format fixing
         fixlist = event.split("  ")
         fixlist2 = []
         for x in fixlist:
            if x != "":
               fixlist2.append(x)
         fixlist2[2] = fixlist2[2].replace(" ", "")
         selection = "start"
         while selection != "k":
            print("The Plan")
            print(fixlist2[0]+" "+fixlist2[1]+" "+fixlist2[2]+"-"+fixlist2[3]+" "+fixlist2[4]+" "+fixlist2[5])
            print("\n"*3,"Options\n1. Add to calendar\nb. Back\nk. Kill\n")
            selection = input("Select: ").lower()
            if selection == "1":
               description = input("Write a description for your plan: ")
               plan1 = fixlist2[0]+"#!#"+description
               output = stub.add_to_calendar(calendar_pb2.CalendarInput(plan=plan1, type="Event", check="n", startt=fixlist2[2], endt=fixlist2[3], date=fixlist2[4], price=fixlist2[1], user=user1, dest_id=destid, e_id=eid))
               if output.confirmation == "There are some conflicts, do you want to continue, y/n: ":
                  choice = ""
                  while choice != "y" or choice != "n":
                     choice = input(output.confirmation)
                     if choice == "y":
                        output = stub.add_to_calendar(calendar_pb2.CalendarInput(plan=plan1, type="Event", check="y", startt=fixlist2[2], endt=fixlist2[3], date=fixlist2[4], price=fixlist2[1], user=user1, dest_id=destid, e_id=eid))
                     elif choice == "n":
                        return
                     else:
                        print("Invalid Input")
               else:
                  print(output.confirmation)
                  pause = input("Enter anything to continue: ")
                  return
            elif selection == "b":
               return
            elif selection == "k":
               return "k"
            else:
               print("Invalid Input")
               
               
   def entertainment_planning_client(entertainment, user1, dest1, eid):
      '''Client to plan entertainments'''
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = calendar_pb2_grpc.CalendarStub(channel)
         fixlist = entertainment.split("  ")
         fixlist2 = []
         for x in fixlist:
            if x != "":
               fixlist2.append(x)
         fixlist2[2] = fixlist2[2].replace(" ", "")
         selection = "start"
         while selection != "k":
            print("The Plan")
            print(fixlist2[0]+" "+fixlist2[1]+" "+fixlist2[2])
            print("\n"*3,"Options\n1. Add to calendar\nb. Back\nk. Kill\n")
            selection = input("Select: ").lower()
            if selection == "1":
               #Comment on format for time, 23:59 is as high as it will go, 24:00 doesn't work, that would be 00:00
               timecheck = True
               while timecheck:
                  starttimecheck = True
                  while starttimecheck:
                     starttimeinput = input("Enter a starting time (Format is 12:45, hh:mm): ")
                     starttimesplit = starttimeinput.split(":")
                     if len(starttimesplit) == 2:
                        if Validators.IntCheck(starttimesplit[0]) and Validators.IntCheck(starttimesplit[1]):
                           if int(starttimesplit[0]) < 24 and int(starttimesplit[0]) >= 0:
                              if int(starttimesplit[1]) < 60 and int(starttimesplit[0]) >= 0:
                                 starttimecheck = False
                              else:
                                 print("Your minutes have to be between 0 and 59")
                           else:
                              print("Your hours have to be between 0 and 23")
                        else:
                           print("Either your minutes or hours are not integers, check your formatting")
                     else:
                        print("I think you forgot the : for the format")
                  endtimecheck = True
                  while endtimecheck:
                     endtimeinput = input("Enter a ending time (Format is 12:45, hh:mm): ")
                     endtimesplit = endtimeinput.split(":")
                     if len(endtimesplit) == 2:
                        if Validators.IntCheck(endtimesplit[0]) and Validators.IntCheck(endtimesplit[1]):
                           if int(endtimesplit[0]) < 24 and int(endtimesplit[0]) >= 0:
                              if int(endtimesplit[1]) < 60 and int(endtimesplit[0]) >= 0:
                                 endtimecheck = False
                              else:
                                 print("Your minutes have to be between 0 and 59")
                           else:
                              print("Your hours have to be between 0 and 23")
                        else:
                           print("Either your minutes or hours are not integers, check your formatting")
                     else:
                        print("I think you forgot the : for the format")
                  starttime = datetime.time(int(starttimesplit[0]), int(starttimesplit[1]))
                  endtime = datetime.time(int(endtimesplit[0]), int(endtimesplit[1]))
                  if starttime < endtime:
                     timecheck = False
                  else:
                     print("Your start time is after your end time.")
               datecheck = True
               while datecheck:
                  dateinput = input("Enter a date you would like to do this on (Format is 2023-10-31, yyyy-mm-dd): ")
                  datesplit = dateinput.split("-")
                  today = datetime.date.today()
                  if len(datesplit) == 3:
                     if Validators.IntCheck(datesplit[0]) and Validators.IntCheck(datesplit[1]) and Validators.IntCheck(datesplit[2]):
                        if int(datesplit[1]) > 0 and int(datesplit[1]) <= 12:
                           if int(datesplit[2]) > 0 and int(datesplit[2]) <= 31:
                              try:
                                 date = datetime.date(int(datesplit[0]), int(datesplit[1]), int(datesplit[2]))
                                 datecheck = False
                              except ValueError:
                                 print("This isn't a valid date due to the day not matching what month it is, fx, like putting in 30th of february, february has max 29 on leap years")
                              if date >= today:
                                 pass
                              else:
                                 print("This date has already passed")
                           else:
                              print("Your day is outside of the normal range 1-31")
                        else:
                           print("Your month is not within the normal month range 1-12")
                     else:
                        print("One of your date components isn't a number")
                  else:
                     print("You do not have 2 dashes in your date")
               description = input("Write a description for your plan: ")
               datesplit.reverse()
               datejoin = "/".join(datesplit)
               plan1 = fixlist2[0]+"#!#"+description
               output = stub.add_to_calendar(calendar_pb2.CalendarInput(plan=plan1, type="Entertainment", check="n", startt=starttimeinput, endt=endtimeinput, date=datejoin, price=fixlist2[1], user=user1, dest_id=dest1, e_id=eid))
               if output.confirmation == "There are some conflicts, do you want to continue, y/n: ":
                  choice = ""
                  while choice != "y" or choice != "n":
                     choice = input(output.confirmation)
                     if choice == "y":
                        output = stub.add_to_calendar(calendar_pb2.CalendarInput(plan=plan1, type="Entertainment", check="y", startt=starttimeinput, endt=endtimeinput, date=datejoin, price=fixlist2[1], user=user1, dest_id=dest1, e_id=eid))
                        print(output.confirmation)
                        return
                     elif choice == "n":
                        return
                     else:
                        print("Invalid Input")
               else:
                  print(output.confirmation)
                  pause = input("Enter anything to continue: ")
                  return
            elif selection == "b":
               return
            elif selection == "k":
               return "k"
            else:
               print("Invalid Input")


   def plan_client(plan, user1, date1):
      '''Client to remove a users plan'''
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = calendar_pb2_grpc.CalendarStub(channel)
         selection = "start"
         while selection != "k":
            print("\nThe Plan\n\n")
            print(plan,"\n\n")
            print("\n"*3,"Options\n1. Remove from calendar\nb. Back\nk. Kill\n")
            selection = input("Select: ").lower()
            if selection == "1":
               output = stub.remove_from_calendar(calendar_pb2.PlanInput(selection=plan, user=user1, date=date1))
               print(output.confirmation,"\n\n")
               pause = input("Enter anything to continue: ")
               return
            elif selection == "b":
               return
            elif selection == "k":
               return "k"
            else:
               print("Invalid Input")
   
   def wipe_calendar_client(user1):
      '''Client to wipe a users calendar'''
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = calendar_pb2_grpc.CalendarStub(channel)
         selection = input("Are you sure you want to delete your calendar plans y/n: ").lower()
         if selection == "y":
            stub.wipe_calendar(calendar_pb2.CalendarWipeInput(user=user1))
            print("Calendar Wiped")
            return
         else:
            print("Calendar Not Wiped")
            return

Main_client.startup_client()
# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from python import changeusername_pb2 as changeusername__pb2


class ChangeNameStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.change_name = channel.unary_unary(
                '/ChangeName/change_name',
                request_serializer=changeusername__pb2.NewNameInput.SerializeToString,
                response_deserializer=changeusername__pb2.NewNameOutput.FromString,
                )
        self.confirm_name = channel.unary_unary(
                '/ChangeName/confirm_name',
                request_serializer=changeusername__pb2.ConfirmNameInput.SerializeToString,
                response_deserializer=changeusername__pb2.ConfirmNameOutput.FromString,
                )


class ChangeNameServicer(object):
    """Missing associated documentation comment in .proto file."""

    def change_name(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def confirm_name(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_ChangeNameServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'change_name': grpc.unary_unary_rpc_method_handler(
                    servicer.change_name,
                    request_deserializer=changeusername__pb2.NewNameInput.FromString,
                    response_serializer=changeusername__pb2.NewNameOutput.SerializeToString,
            ),
            'confirm_name': grpc.unary_unary_rpc_method_handler(
                    servicer.confirm_name,
                    request_deserializer=changeusername__pb2.ConfirmNameInput.FromString,
                    response_serializer=changeusername__pb2.ConfirmNameOutput.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'ChangeName', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class ChangeName(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def change_name(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/ChangeName/change_name',
            changeusername__pb2.NewNameInput.SerializeToString,
            changeusername__pb2.NewNameOutput.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def confirm_name(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/ChangeName/confirm_name',
            changeusername__pb2.ConfirmNameInput.SerializeToString,
            changeusername__pb2.ConfirmNameOutput.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from python import favoritedestination_pb2 as favoritedestination__pb2


class FavoriteDestinationStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.newfavorite = channel.unary_unary(
                '/FavoriteDestination/newfavorite',
                request_serializer=favoritedestination__pb2.FavoriteInput.SerializeToString,
                response_deserializer=favoritedestination__pb2.FavoriteOutput.FromString,
                )
        self.deletefavortie = channel.unary_unary(
                '/FavoriteDestination/deletefavortie',
                request_serializer=favoritedestination__pb2.DeleteFavoriteInput.SerializeToString,
                response_deserializer=favoritedestination__pb2.DeleteFavoriteOutput.FromString,
                )
        self.getfavorites = channel.unary_unary(
                '/FavoriteDestination/getfavorites',
                request_serializer=favoritedestination__pb2.GetFavoriteInput.SerializeToString,
                response_deserializer=favoritedestination__pb2.GetFavoriteOutput.FromString,
                )


class FavoriteDestinationServicer(object):
    """Missing associated documentation comment in .proto file."""

    def newfavorite(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def deletefavortie(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def getfavorites(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_FavoriteDestinationServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'newfavorite': grpc.unary_unary_rpc_method_handler(
                    servicer.newfavorite,
                    request_deserializer=favoritedestination__pb2.FavoriteInput.FromString,
                    response_serializer=favoritedestination__pb2.FavoriteOutput.SerializeToString,
            ),
            'deletefavortie': grpc.unary_unary_rpc_method_handler(
                    servicer.deletefavortie,
                    request_deserializer=favoritedestination__pb2.DeleteFavoriteInput.FromString,
                    response_serializer=favoritedestination__pb2.DeleteFavoriteOutput.SerializeToString,
            ),
            'getfavorites': grpc.unary_unary_rpc_method_handler(
                    servicer.getfavorites,
                    request_deserializer=favoritedestination__pb2.GetFavoriteInput.FromString,
                    response_serializer=favoritedestination__pb2.GetFavoriteOutput.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'FavoriteDestination', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


# This class is part of an EXPERIMENTAL API.
class FavoriteDestination(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def newfavorite(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/FavoriteDestination/newfavorite',
            favoritedestination__pb2.FavoriteInput.SerializeToString,
            favoritedestination__pb2.FavoriteOutput.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def deletefavortie(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/FavoriteDestination/deletefavortie',
            favoritedestination__pb2.DeleteFavoriteInput.SerializeToString,
            favoritedestination__pb2.DeleteFavoriteOutput.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def getfavorites(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/FavoriteDestination/getfavorites',
            favoritedestination__pb2.GetFavoriteInput.SerializeToString,
            favoritedestination__pb2.GetFavoriteOutput.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

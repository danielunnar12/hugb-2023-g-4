import os.path

class Events:
    """Class for the events interface that only contains one function that returns the desired events"""
    def get_events(choice, file="Events.csv"):
        '''Takes in users choice and returns events according to that choice

        args : string (choice, choice is a string that has multiple numbers joined together with a dash, we split and check the number to know what to return)

        returns : string (desired events based on the input and the corresponding id (split with '-'), formatted so that it can just be printed in the client)
        '''
        #Open the file on read
        file = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "r")
        #Create a count and a list and start a for loop
        count = 0
        eventlist = []
        for line in file:
            #Use the count once to make sure we don't take the header
            if count == 0:
                count += 1
            #Then we start working with the events
            else:
                #First we have all the information
                #Then send each line into the list
                #Takes only where destination ID is the same as the destination the user has chosen
                if choice.split('-')[-1]==line.split(',')[1]:
                    eventlist.append(line[:-1])
        #Everything is then done so we close the file
        file.close()
        #The lines of events is then transformed into a string with '!' seperating each line
        string = ''
        for ev in eventlist:
            string+= ev + '!'
        string = string[:-1]
        finalstring='{:<16} {:<16} {:<16} {:<16} {:<16} {}--'.format('Name', 'Price', 'Starting Time', 'Finishing Time', 'Date','Accessibility')
        #Checks first if the city has any events, if the city has no events it return a string explaining so.
        if string == '':
            finalstring = 'This City Has No Events.'
        #Takes the string created before and works with it accordingly
        else:
            if choice.split('-')[0] == '1':
                for line in string.split('!'):
                    acc=''
                    for accnr in range(len(line.split(',')[8].split('-'))-2):
                        acc+= line.split(',')[8].split('-')[accnr+1]+' & '
                    formattedstring = '{:<16} {:<16} {:<16} {:<16} {:<16} {}{}'.format(line.split(',')[3], line.split(',')[4], line.split(',')[5], line.split(',')[6], line.split(',')[7], acc[:-3],'-'+line.split(',')[0]+'-')
                    finalstring+=formattedstring
            elif choice.split('-')[0] == '2':
                for line in string.split('!'):
                    acc=''
                    for accnr in range(len(line.split(',')[8].split('-'))-2):
                        acc+= line.split(',')[8].split('-')[accnr+1]+' & '
                    if int(line.split(',')[4][1:]) >= int(choice.split('-')[1]) and int(line.split(',')[4][1:]) <= int(choice.split('-')[2]):
                        formattedstring = '{:<16} {:<16} {:<16} {:<16} {:<16} {}{}'.format(line.split(',')[3], line.split(',')[4], line.split(',')[5], line.split(',')[6], line.split(',')[7], acc[:-3],'-'+line.split(',')[0]+'-')
                        finalstring += formattedstring
                if finalstring == '{:<16} {:<16} {:<16} {:<16} {:<16} {}--'.format('Name', 'Price', 'Starting Time', 'Finishing Time', 'Date','Accessibility'):
                    finalstring = "No events in price range."
            elif choice.split('-')[0] == '3':
                for line in string.split('!'):
                    acc=''
                    for accnr in range(len(line.split(',')[8].split('-'))-2):
                        acc+= line.split(',')[8].split('-')[accnr+1]+' & '
                    if int(line.split(',')[4][1:]) == int(choice.split('-')[1]):
                        formattedstring = '{:<16} {:<16} {:<16} {:<16} {:<16} {}{}'.format(line.split(',')[3], line.split(',')[4], line.split(',')[5], line.split(',')[6], line.split(',')[7], acc[:-3],'-'+line.split(',')[0]+'-')
                        finalstring += formattedstring
                if finalstring == '{:<16} {:<16} {:<16} {:<16} {:<16} {}--'.format('Name', 'Price', 'Starting Time', 'Finishing Time', 'Date','Accessibility'):
                    finalstring = "No events equal to price."
            elif choice.split('-')[0] == '4':
                for line in string.split('!'):
                    acc=''
                    for accnr in range(len(line.split(',')[8].split('-'))-2):
                        acc+= line.split(',')[8].split('-')[accnr+1]+' & '
                    if choice.split('-')[1] in line.split(',')[8]:
                        formattedstring = '{:<16} {:<16} {:<16} {:<16} {:<16} {}{}'.format(line.split(',')[3], line.split(',')[4], line.split(',')[5], line.split(',')[6], line.split(',')[7], acc[:-3],'-'+line.split(',')[0]+'-')
                        finalstring += formattedstring
                if finalstring == '{:<16} {:<16} {:<16} {:<16} {:<16} {}--'.format('Name', 'Price', 'Starting Time', 'Finishing Time', 'Date','Accessibility'):                
                    finalstring = "No events with chosen filter."
        return finalstring
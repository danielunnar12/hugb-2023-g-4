'''Validators'''
class Validators():
    def __init__(self):
        pass


    def validate_email(email):
        '''Takes in users email and confirms it follows a valid format

        Args: 
            string (email)

        Returns: 
            boolean (to confirm it follows the format)
        '''
        if len(email) < 12 or len(email) > 30:
            return False

        else:
            att = 0
            ending = 0
            for letter in email:
                if letter == "@":
                    att += 1
            if email[-3:] == ".is":
                ending += 1
            elif email[-4:] == ".com":
                ending += 1
            elif email[-4:] == ".net":
                ending += 1
                    
                    
            if att != 1 or ending != 1:
                return False
            else:
                return True
    

    def validate_password(password):
        '''Takes in users password input and confirms it follows a valid format

        Args:
            string (password)

        Returns: 
            boolean (to confirm it follows the format)
        '''
        if len(password) >= 10 and len(password) <= 36:
            capital_letter = 0
            for letter in password:
                if letter.isupper() == True:
                    capital_letter += 1
            if capital_letter > 0:
                return True
            else:
                return False
        else:
            return False


    def validate_username(username):
        '''Takes in username and confirms it follows a valid length

        Args:
            string (username)

        Returns: 
            boolean (to confirm it follows a valid format)
        '''
        if len(username) > 2 and len(username) < 36:
            return True
        else:
            return False
    
    
    def IntCheck(number):
        '''Takes in string and confirms if it is an integer

        Args: 
            string (price)

        Returns: 
            boolean (to confirm it is an integer)
        '''
        try:
            int(number)
            return True
        except:
            return False
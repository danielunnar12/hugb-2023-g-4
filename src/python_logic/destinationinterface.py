import os.path
import os, sys
sys.path.insert(0, os.path.abspath("../.."))
from python_logic.startupinterface import StartupInterface

class DestinationInterface():
    '''Data Functions - Destinations'''
    def get_destinations(input, file="destinations.csv"):
        """Takes in input from user and returns destination depending on input

        Args: 
            input : string (input is a string integer from 1 to 4 that represents type of filter, if input is 3, it is followed by a dash and a city search ex 3-Vienna)
            file : string (default is normal destinations csv, for testing purposes we have this so there we can call a test csv file)

        returns : string (list of destinations that has been joined together into a string)"""
        #Create a data list to store the data from the file
        data = []
        #Open the file
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "r")
        count = 0
        #Go over each line
        for line in f:
            #If count is 0 so for the first line, skip because we don't want the header
            if count == 0:
                count += 1
            #Otherwise, split the line into a list and add it to the data
            else:
                temp = line.split(",")
                data.append(temp)
        #Close the file
        f.close()
        file2 = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), 'destination_reviews.csv'), "r")
        reviewfile = []
        for line in file2:
            reviewfile.append(line)
        file2.close()

        #Create a new list to add what we want from destinations to list
        destList = []
        #Go over the data
        for dest in data:
            #Create a temp list
            tempList = []
            #Add to it city
            tempList.append(dest[1])
            #country
            tempList.append(dest[3])
            #coordinates
            tempList.append(dest[2])
            #Rating and strip newline
            tempList.append(dest[4].rstrip())
            #Adding id for user usage
            tempList.append(dest[0])
            #Add the temp list to our new list
            destList.append(tempList)
        #If input is 1, we are supposed to sort by city
        if input == "1":
            #We use key=lambda to sort by a specific element, in this case x[0] is city
            sortedList = sorted(destList, key=lambda x:x[0])
        elif input == "2":
            #Similarly here, x[1] is country
            sortedList = sorted(destList, key=lambda x:x[1])
        #We use input[0] here because if he searches, we get 3-Amsterdam, meaning we need to call input[0] to get 3
        elif input[0] == "3":
            #Split the string into 3 and search
            search = input.split("-")
            #Create string with just the search for ease of use
            searchstring = search[1].lower()
            #Create a list for the search
            sortedList = []
            #Go over each destination
            for dest in destList:
                #If the string appears in a city (lowercase for ease of use)
                if searchstring in dest[0].lower():
                    #We add it to the list
                    sortedList.append(dest)
            #If nothing is found, we return the whole list sorted by city
            if len(sortedList) == 0:
                sortedList = sorted(destList, key = lambda x:x[0])
            sortedList = sorted(sortedList, key=lambda x:x[0])
        #If input is 4, we sort by rating but also we sort by city name afterwards
        elif input == "4":
            #To do so, we sort the list with key lambda again but a slight issue appears here as we want the numbers going from higher to lower but characters from lower to higher
            #So we sort ordinarily but change the number (rating) to a negative number so the higher number 5 fx is smaller than 4 and is ahead of it because of this.
            sortedList = sorted(destList, key=lambda x : (-int(x[3]), x[0]))
        elif input == '5':
            count2=0
            for dest in destList:
                for review in reviewfile:
                    if dest[4]==review.split(',')[1]:
                        count+=1
                destList[count2].append(str(count))
                count2+=1
                count=0
            sortedList = sorted(destList, key=lambda x : (-int(x[5]),-int(x[3]), x[0]))
        #Because we aren't familiar with proto and gRPC we use strings to move data in between the client and server
        #So we create a listtostr list
        listtostr = []
        #Join each list in the sorted list and add it to the listtostr
        for dest in sortedList:
            desttostr = ",".join(dest)
            listtostr.append(desttostr)
        #Then join the listtostr to a string
        listtostr2 = "!".join(listtostr)    
        #Then return in
        return listtostr2

    def add_favorite_destination(email, new_destination, file="users.csv"):
        """Adds a destination to a users favorite

        Args:
            email : string (user's email)
            new_destination : string (destination that is being added to favorites)
            file : string (default is the normal file, we always use the default except for testing)

        Returns: 
            string (confirmation of adding it, can also be that the user has it already favorited, in which case, that error is returned to him)
        """
        #Get all user info
        user_information = StartupInterface.get_user_info(file)
        #For every user
        for user_creds in user_information:
            #If the email matches our given email
            if user_creds[0] == email:
                #We get his favorite destinations which look something like this 1-2-3-4
                dests = user_creds[3]
                #Split it up by the -
                favoriteDests = dests.split("-")
                #Make a check to check if the list is empty
                check = False
                #If the list is not empty, our check is true and a destination does exist already
                if favoriteDests[0] != "":
                    check = True
                #If the list is empty we just say the string that is his favorite destinations is the new favorite destination
                if check == False:
                    dests = new_destination
                #If it isn't empty, we append to the list the destination
                elif check:
                    if new_destination in favoriteDests:
                        return "Already Favorited"
                    else:
                        favoriteDests.append(new_destination)
                        #And join it into a string afterwards to store as data
                        dests = "-".join(favoriteDests)
                #Then we say the user creds[3] which is the destinations is equal to the string
                user_creds[3] = dests
        #Then we write it to the file.
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w+")  # opening the file with w+ clears the file
        f.close()
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w") as users:
            users.write("email"+","+"password"+","+"username"+","+"fav_dest_ids"+","+"fav_entmt_ids"+"\n")
            for user_creds in user_information:
                email = user_creds[0]
                password = user_creds[1]
                username = user_creds[2]
                fav_dest_ids = user_creds[3]
                fav_entmt_ids = user_creds[4]
                users.write(email+","+password+","+username+","+fav_dest_ids+","+fav_entmt_ids+"\n")

            users.close()
        return "Added to favorites"
    

    def delete_favorite_destination(email, destination, file="users.csv"):
        """Deletes location from a user's favorites

        Args: 
            email : string (user's email)
            destination : string (destination to be deleted)
            file : string (default is the normal file, we always use the default except for testing)

        Returns:
            string (confirmation of deletion)
        """
        #Get all user information
        user_information = StartupInterface.get_user_info(file)
        #For each user
        for user_creds in user_information:
            #If that user's email matches our user's email
            if user_creds[0] == email:
                #We get the favorite destinations of that user
                dests = user_creds[3]
                #Split it up into a list
                favoriteDests = dests.split("-")
                #Go over those destinations with a count
                count = 0
                for dest in favoriteDests:
                    #If the id equals our given id
                    if str(dest) == destination:
                        #We grab the index
                        index = count
                    #Add to the count
                    count += 1
                #Pop the destination from the list
                favoriteDests.pop(index)
                #If the length is 0, we reset his favorites for the csv file
                if len(favoriteDests) == 0:
                    favdests = "-"
                #Otherwise
                else:
                    #We just join them back together
                    favdests = "-".join(favoriteDests)
                #Change the user information
                user_creds[3] = favdests
        #And write it to the file
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w+")  # opening the file with w+ clears the file
        f.close()
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w") as users:
            users.write("email"+","+"password"+","+"username"+","+"fav_dest_ids"+","+"fav_entmt_ids"+"\n")
            for user_creds in user_information:
                email = user_creds[0]
                password = user_creds[1]
                username = user_creds[2]
                fav_dest_ids = user_creds[3]
                fav_entmt_ids = user_creds[4]
                users.write(email+","+password+","+username+","+fav_dest_ids+","+fav_entmt_ids+"\n")
        
        
            users.close()


    def get_destination_reviews(file="destination_reviews.csv"):
        """Get all reviews of all destinations
        Args:
            file (str): Predetermined file which should store the reviews for all destinations
            
        Returns:
            destination_review_list (list of lists): Returns all reviews for all destinations in list of lists form. Each review list looks like this [email, dest_id, review, username, rating]
        """
        destination_review_list = []
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "r") as reviews:
            count = 0
            for line in reviews:
                if count == 0:
                    count += 1
                else:
                    temp = line.split(",")
                    temp_strp = []
                    for element in temp:
                        temp_strp.append(element.strip()) 
                    destination_review_list.append(temp_strp)
                    count += 1
        reviews.close
        return destination_review_list


    def get_reviews_on_destination(dest_id, file="destination_reviews.csv"):
        """Get all reviews for one selected location
        
        Args:
            dest_id (str): Id of the destination we want to get reviews for
            file (str): Predetermined file that contains all reviews
        
        Returns:
            reviews_str (str): Returns a string that contains all reviews on the destination
            
        Note:
            Note that the reviews are lists that are with joined with !-!_! so when you want to work with the information you must split on !-!_!
        """
        all_reviews = DestinationInterface.get_destination_reviews(file)
        reviews_on_dest = []
        for destination_review in all_reviews:
            if destination_review[1] == str(dest_id):
                reviews_on_dest.append(destination_review)
        reviews_on_dest_str = []
        for review in reviews_on_dest:
            review_to_str = ",".join(review)
            reviews_on_dest_str.append(review_to_str)
        reviews_str = "!-!_!".join(reviews_on_dest_str)
        return reviews_str
    

    def get_user_reviews(user, file="destination_reviews.csv"):
        """Get every review that this user has made
        
        Args:
            user (str): Email(ID) of the user
            file (str): Predetermined file which contains all reviews for all destinations
        
        Returns:
            reviews_str (str): Returns a string that contains all reviews that the user has made
        
        Note:
            Note that the reviews are lists that are with joined with !-!_! so when you want to work with the information you must split on !-!_!
        """
        all_reviews = DestinationInterface.get_destination_reviews(file)
        user_reviews = []
        for destination_review in all_reviews:
            if destination_review[0] == str(user):
                user_reviews.append(destination_review)
        user_reviews_str = []
        for review in user_reviews:
            review_to_str = ",".join(review)
            user_reviews_str.append(review_to_str)
        reviews_str = "!-!_!".join(user_reviews_str)
        return reviews_str


    
    def review_already_exists(email, dest_id, file="destination_reviews.csv"):
        """Checks if the user has already left a review for the location (We do not want more than one review from the same user on the same location)
        
        Args:
            email (str): Email(ID) of the user
            dest_id (str): Destination ID
            file (str): Predetermined file containing all reviews on all destinations
        
        Returns:
            x (boolean): return True if review for location already exists, else return False
        """
        destination_reviews = DestinationInterface.get_destination_reviews(file) # get every review on all destinations we have
        x = False
        for review in destination_reviews:
            if review[0] == email and review[1] == dest_id: # checks if the user has already left a review for this destination
                x = True # Review exists
        return x # Review doesn't exist


    def make_review(email, dest_id, review, rating="5", file="destination_reviews.csv", file2="users.csv", file3="destinations.csv"):
        """Add a review made by a user to the destination_reviews.csv file
        
        Args:
            email (str): Email(ID) of the user
            dest_id (str): Destination ID
            review (str): Input from user formatted as a string
            rating (str): String in range 1-5 (1, 2, 3, 4 or 5)
            file (str): Predetermined file which contains all reviews for all destinations
            file2 (str): Predetermined file that contains information on all of our users
            file3 (str): Predetermined file that contains information about all our destinations
        
        Returns:
            boolean: If there is no review from this user on this destination - create the review and add it to our reviews file, else - return False
        
        Note: 
            When the review is inserted into the file we replace "," with ";" because the categories in our csv files are seperated with ","
        """
        user_info = StartupInterface.get_user_info(file2) # gets the information from our users
        for user_creds in user_info:
            if user_creds[0] == email:
                username = user_creds[2] # finds the username of the user to attach to the review
        review = str(review).replace(",",";")
        if DestinationInterface.review_already_exists(email, dest_id, file) == True: #
            return False
        else:
            with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "a") as reviews:
                reviews.write(email+","+dest_id+","+review+","+username+","+rating+"\n")
                reviews.close()
            # for tests
            if file == "test_destination_reviews.csv":
                pass
            else:
                DestinationInterface.update_destination_rating(dest_id, file3, file)
            return True


    def edit_review(email, dest_id, new_review, rating="null", file="destination_reviews.csv", file2="destinations.csv"):
        """Takes already existing review and replaces the review category with a new string and also the rating if the that option was chosen.

        Args:
            email (str): Email(ID) of the user
            dest_id (str): Destination ID
            review (str): Input from user formatted as a string
            rating (str): String in range 1-5 (1, 2, 3, 4 or 5)
            file (str): Predetermined file which contains all reviews for all destinations
            file2 (str): Predetermined file that contains information about all our destinations
        
        Returns:
            x (boolean): If review exists edit it - return True, else - return False
        """
        new_review = str(new_review).replace(",",";") # Replaces all instances of , with ; so that when we fetch the data it splits correctly
        all_reviews = DestinationInterface.get_destination_reviews(file) # Returns all reviews in list of lists format
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w+")  # opening the file with w+ clears the file
        f.close()
        x = False
        for destination_review in all_reviews:
            if destination_review[0] == email and destination_review[1] == dest_id: # locate the already existing review
                destination_review[2] = str(new_review)
                if rating != "null":
                    destination_review[4] = rating
                x = True
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w") as reviews:
            '''Rewriting our destination_reviews.csv file with the new review'''
            reviews.write("email"+","+"destination_id"+","+"review"+","+"username"+","+"rating"+"\n")
            for destination_reviews in all_reviews:
                email = destination_reviews[0]
                destination_id = destination_reviews[1]
                review = destination_reviews[2]
                username = destination_reviews[3]
                rating = destination_reviews[4]
                reviews.write(email+","+destination_id+","+review+","+username+","+rating+"\n")
            reviews.close()
        if file == "test_destination_reviews.csv":
            pass
        else:
            DestinationInterface.update_destination_rating(dest_id, file2, file)
        return x
                

    def delete_review(email, dest_id, file="destinations.csv", file2 = "destination_reviews.csv"):
        """Deletes an already existing review from our reviews database
        
        Args:
            email (str): Email(ID) of the user
            dest_id (str): Destination ID
            file (str): Predetermined file that contains information about all our destinations
            file2 (str): Predetermined file which contains all reviews for all destinations
        
        Returns:
            x (boolean): If review exists delete it - return True, else - return False
        """
        destination_reviews = DestinationInterface.get_destination_reviews(file2)
        count = 0
        x = False
        for review in destination_reviews:
            count +=1
            if email == review[0] and dest_id == review[1]:
                destination_reviews.pop(count-1)
                x = True
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file2), "w+") # opening the file with w+ clears the file
        f.close()
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file2), "w") as users:
            '''Rewriting our users.csvfile with the user deleted '''
            if destination_reviews != []:
                users.write("email"+","+"destination_id"+","+"review"+","+"username"+","+"rating"+"\n")
                for review in destination_reviews:
                    email = review[0]
                    destination_id = review[1]
                    user_review = review[2]
                    username = review[3]
                    rating = review[4]
                    users.write(email+","+destination_id+","+user_review+","+username+","+rating+"\n")
                users.close()
            else:
                users.write("email"+","+"destination_id"+","+"review"+","+"username"+","+"rating"+"\n")
        if file2 == "test_destination_reviews.csv":
            pass
        else:
            DestinationInterface.update_destination_rating(dest_id, file, file2)
        return x
    

    def update_destination_rating(dest_id, file="destinations.csv", file2="destination_reviews.csv"):
        """Updates the rating of a destination after a review is made
        
        Args:
            dest_id (str): Destination ID
            file (str): Predetermined file that contains information about all our destinations
            file2 (str): Predetermined file which contains all reviews for all destinations
        
        Returns:
            Nothing to return
        
        Note:
            Looks at all ratings given for the chosen destination, sums them, divides the sum by count of reviews to get mean (xbar), round to the nearest integer and replace the old rating with that new one. 
        """
        all_destinations = DestinationInterface.get_destinations("1", file) # Returns all destinations sorted by city in string format 
        all_reviews = DestinationInterface.get_destination_reviews(file2) # Returns destination reviews in list of lists format
        count = 0
        ratings_total = 0
        for review in all_reviews:
            if review[1] == dest_id:
                ratings_total += int(review[4])
                count += 1
        if count == 0:
            pass
        else:
            # Necessary since round function rounds differently depending on if the number is even or odd
            new_rating = ratings_total/count
            if new_rating > 1.49 and new_rating < 1.51:
                new_rating = new_rating // 1 + 1
            elif new_rating > 2.49 and new_rating < 2.51:
                new_rating = new_rating // 1 + 1
            elif new_rating > 3.49 and new_rating < 3.51:
                new_rating = new_rating // 1 + 1
            elif new_rating > 4.49 and new_rating < 4.51:
                new_rating = new_rating // 1 + 1
            dests = all_destinations.split("!") # Changing all destinations str to list of lists
            count = 0
            for dest in dests:
                destin = dest.split(",")
                dests[count] = destin
                count += 1
            for dest in dests:
                if dest[4] == dest_id:
                    dest[3] = str(int(new_rating))
            sortedList = sorted(dests, key=lambda x:int(x[4])) # Need to sort again by id to put it back in the file in the correct order
            f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w+") # opening the file with w+ clears the file
            f.close()
            # Rewriting the file with the changed rating
            with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w") as destinations:
                if dests != []:
                    destinations.write("id"+","+"city"+","+"coordinates"+","+"country"+","+"rating"+"\n")
                    for dest in sortedList:
                        dest_id = dest[4]
                        city = dest[0]
                        coordinates = dest[2]
                        country = dest[1]
                        rating = dest[3]
                        destinations.write(dest_id+","+city+","+coordinates+","+country+","+rating+"\n")
                    destinations.close()
                else:
                    destinations.write("id"+","+"city"+","+"coordinates"+","+"country"+","+"rating"+"\n")
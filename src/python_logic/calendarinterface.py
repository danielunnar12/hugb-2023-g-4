import datetime
import os, sys
import os.path
import calendar
class CalendarInterface:
    def get_month(year, month, email):
        '''Creates a calendar of given month and year
        
        args: 
        year: string
        month: string
        email: string
        file: string (default because of testing)
        
        returns:
        Calendar in one string with user's plans marked with * before the days
        '''
        #A lot of comments will be here to explain this as understanding it from just looking at it might be difficult.
        #The calendar package does have its own calendar but we wanted to be able to show the user which days have something going on. So we made our own calendar that can be customized.
        year = int(year)
        month = int(month)
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "r") as userdays:
            #Create a count to skip first line and a list to store the information
            count = 0
            userdayslist = []
            for line in userdays:
                if count == 0:
                    count += 1
                else:
                    templist = line.split(",")
                    #Change the ; in the descriptions to commas, this is kept like this to keep the Comma Separated Value working 
                    templist[3] = templist[3].replace(";", ",")
                    if templist[1] == email:
                        userdayslist.append(templist)
        #List of dates the user has plans.
        eventdates = []
        for userday in userdayslist:
            date = userday[2]
            datelist = date.split("-")
            dateyear = int(datelist[0])
            datemonth = int(datelist[1])
            dateday = int(datelist[2])
            date = datetime.date(dateyear, datemonth, dateday)
            eventdates.append(date)
        #String formatting, to make it look nice.
        finalstring = ""
        monthname = calendar.month_name[month]
        monthrange = calendar.monthrange(year, month)
        weekdays = "Monday".center(20)+"Tuesday".center(20)+"Wednesday".center(20)+"Thursday".center(20)+"Friday".center(20)+"Saturday".center(20)+"Sunday".center(20)
        monthandyear = monthname+" "+str(year)
        monthandyear = monthandyear.center(140)
        finalstring = finalstring+monthandyear+"\n"
        finalstring = finalstring+weekdays+"\n"
        #Create 3 different kinds of counts
        #Daycount keeps track of day of month, for example, 26th of October would be daycount 26
        daycount = 0
        #Weekdaycount keeps track of weekdays, for example, 26th of October is a thursday and this helps keep track of that, you'll see why later.
        weekdaycount = 0
        #This last count is to match the weekdaycount to a weekday number on a date, it'll become clear later
        weeks = 0
        while daycount != monthrange[1]:
            date = datetime.date(year, month, daycount+1)
            weekday = date.weekday()
            #Weekday returns a count from 0 to 6, 0 is monday, 6 is sunday, you get the rest
            #This helps us keep track of when a day should be printed and when not.
            #Here we say weekday+weeks because weekday is from 0 to 6 but our count will go above that after the first week, so everytime we pass a sunday, we add 7 to weeks and use it here.
            if weekdaycount == weekday+weeks:
                if date in eventdates:
                    #Formatting
                    string = str(daycount+1)
                    string = "*"+string
                    finalstring = finalstring+string.center(20)+""
                else:
                    finalstring = finalstring+str(daycount+1).center(20)+""
                daycount += 1
            else:
                #Formatting
                finalstring = finalstring+"".center(20)+""
            #Add 7 to weeks if a week passes and make a newline for the next week
            if weekdaycount == 6 or weekdaycount == 13 or weekdaycount == 20 or weekdaycount == 27 or weekdaycount == 34:
                weeks += 7
                finalstring = finalstring+"\n"
            weekdaycount += 1
        return finalstring


    def get(year, month, day, email):
        '''Gets plans of a user for a certain date
        
        args:
        year : string 
        month : string
        day : string
        email : string
        
        returns:
        String of all plans for a user on a certain date
        '''
        file = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "r")
        finalstring='{:<16} {:<16} {:<8} {:<16} {}\n'.format('Starting Time','Finishing Time','Price','Type','Description')
        ids = []
        for line in file:
            list = line.split(',')
            
            datesplit = list[2].split('-')
            if list[1] == email and datesplit[0]==year and datesplit[1]==month and datesplit[2]==day:
                e_id = list[5]
                ids.append(e_id)
                if len(list[3])>60:
                    desc=list[3][:60]+'...'
                else:
                    desc = list[3]
                formattedstring='{:<16} {:<16} {:<8} {:<16} {:}\n'.format(list[6],list[7],list[8][:-1],list[4],desc)
            else:
                formattedstring=''
            finalstring+=formattedstring
        finalstring = finalstring[:-1]
        strids = "/-/".join(ids)
        finalstring = finalstring.replace(";", ",")
        finalstring = finalstring+"_!##_"+strids
        return finalstring

#Needs User
    def add_to_calendar(plan, type, check, price, date, starttime, endtime, user, destid, e_id):
        '''Adds a plan to a users calendar
        
        args: 
        plan : string (name + user description)
        type : string (Event/Entertainment)
        check : string (either y or n for yes and no, it starts with no, and if a conflict happens, ask him to confirm and then y goes in to override conflicts)
        price : string (price of plan)
        date : string (date of plan)
        starttime : string (starting time of plan)
        endtime : string (ending time of plan)

        returns : string (confirmation of plan)
        '''
        file = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "r")
        planlist = []
        for line in file:
            templist = line.split(",")
            planlist.append(templist)
        file.close()
        file = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "Events.csv"), "r")
        eventlist = []
        for line in file:
            templist = line.split(",")
            eventlist.append(templist)
        file.close()
        file = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "entertainments.csv"), "r")
        entertainmentlist = []
        for line in file:
            templist = line.split(",")
            entertainmentlist.append(templist)
        file.close()
        plan = plan.split("#!#")
        ename = plan[0]
        description = plan[1]
        description = description.replace(",", ";")
        if type == "Entertainment":
            starttimesplit = starttime.split(":")
            endtimesplit = endtime.split(":")
            datesplit = date.split("/")
            starttimefix = datetime.time(int(starttimesplit[0]), int(starttimesplit[1]))
            endtimefix = datetime.time(int(endtimesplit[0]), int(endtimesplit[1]))
            datefix = datetime.date(int(datesplit[2]), int(datesplit[1]), int(datesplit[0]))
            if check != "y":
                count = 0
                check = True
                for plans in planlist:
                    if count == 0:
                        pass
                        count += 1
                    else:
                        if plans[1] == user:
                            if plans[4] == description:
                                return "There are some conflicts, do you want to continue, y/n : "
                            planstart = plans[6]
                            planend = plans[7]
                            plandate = plans[2]
                            planstartsplit = planstart.split(":")
                            planendsplit = planend.split(":")
                            plandatesplit = plandate.split("-")
                            planstartfix = datetime.time(int(planstartsplit[0]), int(planstartsplit[1]))
                            hour = int(planendsplit[0])
                            minute = int(planendsplit[1])
                            planendfix = datetime.time(hour, minute)
                            plandatefix = datetime.date(int(plandatesplit[0]), int(plandatesplit[1]), int(plandatesplit[2]))
                            if datefix == plandatefix:
                                if (starttimefix > planstartfix or starttimefix < planendfix) or (endtimefix > planstartfix or endtimefix < planendfix):
                                    return "There are some conflicts, do you want to continue, y/n : "
                            if plans[6] == e_id:
                                return "There are some conflicts, do you want to continue, y/n : "
                if check == True:
                    newplan = []
                    newplan.append(str(len(planlist)))
                    newplan.append(user)
                    datejoin = datesplit[2]+"-"+datesplit[1]+"-"+datesplit[0]
                    newplan.append(datejoin)
                    newplan.append(description)
                    newplan.append("Entertainment")
                    newplan.append(e_id)
                    newplan.append(starttime)
                    newplan.append(endtime)
                    newplan.append(price)
                    planlist.append(newplan)
                        
            else:
                newplan = []
                newplan.append(str(len(planlist)))
                newplan.append(user)
                datejoin = datesplit[2]+"-"+datesplit[1]+"-"+datesplit[0]
                newplan.append(datejoin)
                newplan.append(description)
                newplan.append("Entertainment")
                newplan.append(e_id)
                newplan.append(starttime)
                newplan.append(endtime)
                newplan.append(price)
                planlist.append(newplan)
            f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "w+") # opening the file with w+ clears the file
            f.close()
            with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "w") as plans:
                for plan in planlist:
                    plans.write(plan[0]+","+plan[1]+","+plan[2]+","+plan[3]+","+plan[4]+","+plan[5]+","+plan[6]+","+plan[7]+","+plan[8])
                plans.write("\n")
                plans.close()
            return "Plan added to calendar"
        else:
            starttimesplit = starttime.split(":")
            endtimesplit = endtime.split(":")
            datesplit = date.split("/")
            starttimefix = datetime.time(int(starttimesplit[0]), int(starttimesplit[1]))
            endtimefix = datetime.time(int(endtimesplit[0]), int(endtimesplit[1]))
            datefix = datetime.date(int(datesplit[2]), int(datesplit[1]), int(datesplit[0]))
            if check != "y":
                count = 0
                check = True
                for plans in planlist:
                    if count == 0:
                        pass
                        count += 1
                    else:
                        if plans[1] == user:
                            if plans[4] == description:
                                return "There are some conflicts, do you want to continue, y/n : "
                            planstart = plans[6]
                            planend = plans[7]
                            plandate = plans[2]
                            planstartsplit = planstart.split(":")
                            planendsplit = planend.split(":")
                            plandatesplit = plandate.split("-")
                            planstartfix = datetime.time(int(planstartsplit[0]), int(planstartsplit[1]))
                            hour = int(planendsplit[0])
                            minute = int(planendsplit[1])
                            planendfix = datetime.time(hour, minute)
                            plandatefix = datetime.date(int(plandatesplit[0]), int(plandatesplit[1]), int(plandatesplit[2]))
                            if datefix == plandatefix:
                                if (starttimefix > planstartfix or starttimefix < planendfix) or (endtimefix > planstartfix or endtimefix < planendfix):
                                    return "There are some conflicts, do you want to continue, y/n : "
                            if plans[6] == e_id:
                                return "There are some conflicts, do you want to continue, y/n : "
                if check == True:
                    newplan = []
                    newplan.append(str(len(planlist)))
                    newplan.append(user)
                    datejoin = datesplit[2]+"-"+datesplit[1]+"-"+datesplit[0]
                    newplan.append(datejoin)
                    newplan.append(description)
                    newplan.append("Event")
                    newplan.append(e_id)
                    newplan.append(starttime)
                    newplan.append(endtime)
                    newplan.append(price)
                    planlist.append(newplan)
                        
            else:
                newplan = []
                newplan.append(str(len(planlist)))
                newplan.append(user)
                datejoin = datesplit[2]+"-"+datesplit[1]+"-"+datesplit[0]
                newplan.append(datejoin)
                newplan.append(description)
                newplan.append("Event")
                newplan.append(e_id)
                newplan.append(starttime)
                newplan.append(endtime)
                newplan.append(price)
                planlist.append(newplan)
            f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "w+") # opening the file with w+ clears the file
            f.close()
            with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "w") as plans:
                for plan in planlist:
                    plans.write(plan[0]+","+plan[1]+","+plan[2]+","+plan[3]+","+plan[4]+","+plan[5]+","+plan[6]+","+plan[7]+","+plan[8])
                plans.write("\n")
                plans.close()
            return "Plan added to calendar" 
                
                
    def remove_from_calendar(selection, user, date):
        '''Removes users selection from plans
        
        args:
        selection: string (Entire plan in string)
        user: string (Email)
        
        returns:
        confirmation: string (Confirms deletion)
        '''
        selectionsplit = selection.split("  ")
        fixedlist = []
        for x in selectionsplit:
            if x != "":
                fixedlist.append(x)
        fixedlist[3] = fixedlist[3].replace(" ", "")
        
        file = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "r")
        planlist = []
        for line in file:
            templist = line.split(",")
            planlist.append(templist)
        file.close()
        count = 0
        for plan in planlist:
            if plan[1] == user:
                if plan[2] == date:
                    if plan[3] == fixedlist[4]:
                        if plan[4] == fixedlist[3]:
                            if plan[6] == fixedlist[0]:
                                if plan[7] == fixedlist[1]:
                                    fixedlist[2] = fixedlist[2].replace("\n","")
                                    if plan[8].replace("\n","") == fixedlist[2]:
                                        planlist.pop(count)
                                        break
            count += 1
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "w+") # opening the file with w+ clears the file
        f.close()
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "w") as plans:
            for plan in planlist:
                plans.write(plan[0]+","+plan[1]+","+plan[2]+","+plan[3]+","+plan[4]+","+plan[5]+","+plan[6]+","+plan[7]+","+plan[8])
            plans.close()
        return "Plan removed from calendar" 

    
    def wipe_calendar(user):
        '''Wipes a users calendar
        
        args:
        user: string
        
        returns:
        confirmation: string
        '''
        file = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "r")
        planlist = []
        for line in file:
            templist = line.split(",")
            planlist.append(templist)
        file.close()
        count = 0
        indexes = []
        for plan in planlist:
            if plan[1] == user:
                indexes.append(count)
            count += 1
        newcount = 0
        for x in indexes:
            planlist.pop(x-newcount)
            newcount += 1
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "w+") # opening the file with w+ clears the file
        f.close()
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), "user_days.csv"), "w") as plans:
            for plan in planlist:
                plans.write(plan[0]+","+plan[1]+","+plan[2]+","+plan[3]+","+plan[4]+","+plan[5]+","+plan[6]+","+plan[7]+","+plan[8])
            plans.close()
        return "Calendar Wiped" 
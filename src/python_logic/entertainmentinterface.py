import os.path

class EntertainmentsInterface:
    """Class for the entertainments interface that only contains one function that returns the desired entertainments"""

    def get_entertainments(choice, file="entertainments.csv"):
        '''Takes in users choice and returns entertainments according to that choice

        args : string (choice, choice is a string that has multiple numbers joined together with a dash, we split and check the numbers or length of the string to know what to do)

        returns : string (desired entertainments based on the input, formatted so that it can just be printed in the client with no loops)
        '''     
        open_file = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "r")
        choicesplit = choice.split("-")
        count = 0
        entmtlist = []
        if len(choicesplit) == 2:
            for line in open_file:
                if count == 0:
                    count += 1
                else:
                    x = line.split(",")
                    if x[1] == choicesplit[1]:
                        entmtlist.append(line)
        elif choicesplit[0] == '3':
            price = choicesplit[1]
            for line in open_file:
                if count == 0:
                    count += 1
                else:
                    x = line.split(",")
                    entprice = x[4][1:]
                    if int(price) == int(entprice):
                        if x[1] == choicesplit[2]:
                            entmtlist.append(line)
        elif len(choicesplit) == 4:
            minprice = int(choicesplit[1])
            maxprice = int(choicesplit[2])
            for line in open_file:
                if count == 0:
                    count += 1
                else:
                    x = line.split(",")
                    entprice = int(x[4][1:])
                    if entprice >= minprice and entprice <= maxprice:
                        if x[1] == choicesplit[3]:
                            entmtlist.append(line)
        elif choicesplit[0] == '4':
            for line in open_file:
                x = line.split(",")
                acc = choicesplit[1]
                if acc in line.split(',')[5]:
                    if x[1] == choicesplit[2]:
                        entmtlist.append(line)
        open_file.close()
        string = ''
        for entmt in entmtlist:
            string += entmt + '!'
        string = string[:-1]
        finalstring = '{:<16} {:<16} {}--'.format('Name', 'Price', 'Accessibility')
        #Takes the string created before and works with it accordingly (this is very stupid and will hopefully be fixed later but it works atm)
        if choice.split('-')[0] == '1':
            if string != '':
                for line in string.split('!'):
                    acc=''
                    for accnr in range(len(line.split(',')[5].split('-'))-2):
                        acc+= line.split(',')[5].split('-')[accnr+1]+' & '
                    formattedstring = '{:<16} {:<16} {}{}'.format(line.split(',')[3], line.split(',')[4],acc[:-3],'-'+line.split(',')[0]+'-')
                    finalstring += formattedstring
            if finalstring == '{:<16} {:<16} {}--'.format('Name', 'Price', 'Accessibility'):
                finalstring = "This city has no Entertainments."
        elif choice.split('-')[0] == '2':
            if string != '':
                for line in string.split('!'):
                    acc=''
                    for accnr in range(len(line.split(',')[5].split('-'))-2):
                        acc+= line.split(',')[5].split('-')[accnr+1]+' & '
                    if int(line.split(',')[4][1:]) >= int(choice.split('-')[1]) and int(line.split(',')[4][1:]) <= int(choice.split('-')[2]):
                        formattedstring = '{:<16} {:<16} {}{}'.format(line.split(',')[3], line.split(',')[4],acc[:-3],'-'+line.split(',')[0]+'-')
                        finalstring += formattedstring
            if finalstring == '{:<16} {:<16} {}--'.format('Name', 'Price', 'Accessibility'):
                finalstring = "No entertainments in price range."
        elif choice.split('-')[0] == '3':
            if string != '':
                for line in string.split('!'):
                    acc=''
                    for accnr in range(len(line.split(',')[5].split('-'))-2):
                        acc+= line.split(',')[5].split('-')[accnr+1]+' & '
                    if int(line.split(',')[4][1:]) == int(choice.split('-')[1]):
                        formattedstring = '{:<16} {:<16} {}{}'.format(line.split(',')[3], line.split(',')[4],acc[:-3],'-'+line.split(',')[0]+'-')
                        finalstring += formattedstring
            if finalstring == '{:<16} {:<16} {}--'.format('Name', 'Price', 'Accessibility'):
                finalstring = "No entertainments equal to price."
        elif choice.split('-')[0] == '4':
            if string != '':
                for line in string.split('!'):
                    acc=''
                    for accnr in range(len(line.split(',')[5].split('-'))-2):
                        acc+= line.split(',')[5].split('-')[accnr+1]+' & '
                    if choice.split('-')[1] in line.split(',')[5]:
                        formattedstring = '{:<16} {:<16} {}{}'.format(line.split(',')[3], line.split(',')[4],acc[:-3],'-'+line.split(',')[0]+'-')
                        finalstring += formattedstring
            if finalstring == '{:<16} {:<16} {}--'.format('Name', 'Price', 'Accessibility'):
                finalstring = "No entertainments with chosen filter."
        return finalstring
import os.path

class StartupInterface():
    def __init__(self):
        pass

    '''Data Functions - StartupInterface (Account Details)'''

    def get_login_info(file="users.csv"):
        """
        Read login information from a CSV file and return it as a list of username and password pairs.

        This function opens a specified CSV file, reads its contents, and processes each line.
        Each line in the CSV file is assumed to represent user login information, with comma-separated values.

        Args:
        - file (str): The name of the CSV file to read. Default is "users.csv" in the current directory.

        Returns:
        - list of lists: A list containing login information, where each inner list represents a username and password pair.
        The inner lists contain the first and second values extracted from the CSV file (assuming those are username and password),
        with leading and trailing whitespaces removed.

        Note:
        - The CSV file is expected to have a header row, which is skipped during processing.
        - The CSV file should contain comma-separated values for each user's login information, with the username in the first column
        and the password in the second column.

        """
        login_information = []
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "r") as user_info:
            count = 0
            for line in user_info:
                if count == 0:
                    count += 1
                else:
                    temp = line.split(",")
                    temp_strp = []
                    for element in temp:
                        temp_strp.append(element.strip()) 
                    login_information.append([temp_strp[0], temp_strp[1]])
                    count += 1
        user_info.close()
        return login_information
    

    def get_user_info(file="users.csv"):
        """Read user information from a CSV file and return it as a list of lists.

            This function opens a specified CSV file, reads its contents, and processes each line.
            Each line in the CSV file is assumed to represent a user's information, with comma-separated values.

            Args:
            - file (str): The name of the CSV file to read. Default is "users.csv" in the current directory.

            Returns:
            - list of lists: A list containing user information, where each inner list represents a user's data.
            The inner lists contain individual values extracted from the CSV file, with leading and trailing whitespaces removed.

            Note:
            - The CSV file is expected to have a header row, which is skipped during processing.
            - The CSV file should contain comma-separated values (CSV format) for each user's information.
        """
        user_info_list = []
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "r") as user_info:
            count = 0
            for line in user_info:
                if count == 0:
                    count += 1
                else:
                    temp = line.split(",")
                    temp_strp = []
                    for element in temp:
                        temp_strp.append(element.strip()) 
                    user_info_list.append(temp_strp)
                    count += 1
        user_info.close()
        return user_info_list


    def already_exists(email, file="users.csv"):
        """
        Check if a user with the given email address already exists in a CSV file of login information.

        This function reads login information from a specified CSV file and checks if a user with the provided email address
        already exists in the file.

        Args:
        - email (str): The email address to check for in the CSV file.
        - file (str): The name of the CSV file containing login information. Default is "users.csv" in the current directory.

        Returns:
        - x (boolean): True if a user with the provided email address exists in the CSV file, False otherwise.

        Note:
        - The function uses the provided email to search for a match in the CSV file's usernames (assuming it follows the format
        established in `get_login_info`).
        - The CSV file is expected to have a header row, which is skipped during processing.

        """
        

        login_information = StartupInterface.get_login_info(file)
        x = False
        for usercreds in login_information:
            if usercreds[0] == email:
                x = True
        return x

    '''Account functions'''

    def current_user(file="current_user.csv"):
        """Checks who the current user of the application is.
        
        Args:
            file (str): The name of the CSV file containing login information. Default is "users.csv" in the current directory.

        Returns:
            email (str): the email of the current user
        """
        users = []
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "r") as f:
            count = 0
            for line in f:
                if count == 0:
                    count += 1
                else:
                    email = line.strip()
                    return email
    

    def create_account(email, password, username, file="users.csv"):
        """Creates a new user from the email, password and username entered
        
        Args:
            email (str): Email is the ID for the user, will be used to log in
            password (str): Password for the user, will be used to log in
            username (str): Users display name
        
        Returns:
            Nothing to return
        """
        fav_dest_ids = "-"
        fav_entmt_ids = "-"
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "a") as users:
            users.write(email+","+password+","+username+","+fav_dest_ids+","+fav_entmt_ids+"\n")
            users.close()


    def user_login(email, password, file="current_user.csv", file1="users.csv"):
        """Checks if there is a combination of the entered [email, password] is in our user database
        
        Args:
            email (str): Email entered by user to match a password for log in
            password (str): Password entered by user to match an email for log in
            
        Returns:
            boolean: Returns true and performs a log in if there is a pair of the email and password entered in our database, else return False
            
        Note:
            No emails are the same so there can be only one email and password pair
        """
        

        login_information = StartupInterface.get_login_info(file1)
        if [email, password] in login_information:
            '''opening the file with w+ clears the file'''
            current_user = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w+")  # opening the file with w+ clears the file
            current_user.close()
            with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "a") as current_user :
                '''Only email is needed since that is the id of our users'''
                current_user.write("current_user\n")
                current_user.write(email+"\n")
                current_user.close()
            return True
        else:
            return False
    

    def logout(file="current_user.csv"):
        """Clears the current_user.csv file so there is no current user (logs out the current user)
        
        Args:
            file (str): Gets a predetermined file that contains the email of the current user
        
        Returns:
            Nothing to return
        """
        
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w+")  # opening the file with w+ clears the file
        f.close()


    def change_password(email, new_password, file="users.csv"):
        """Checks if there is a combination of the entered [email, password] in our user database 
        and changes the password in the list of our current user
        
        Args:
            email (str): Email(ID) of our user, used to identify which password to change
            new_password (str): The new_password will replace the old password that matches the users email(ID)
            file (str): Predetermined file of our stored users

        Returns:
            Nothing to return
        """

        user_information = StartupInterface.get_user_info(file)
        for user_creds in user_information:
            if user_creds[0] == email:
                user_creds[1] = str(new_password)
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w+") # opening the file with w+ clears the file
        f.close()
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w") as users:
            '''Rewriting our users.csvfile with the updated password of the user'''
            users.write("email"+","+"password"+","+"username"+","+"fav_dest_ids"+","+"fav_entmt_ids"+"\n")
            for user_creds in user_information:
                email = user_creds[0]
                password = user_creds[1]
                username = user_creds[2]
                fav_dest_ids = user_creds[3]
                fav_entmt_ids = user_creds[4]
                users.write(email+","+password+","+username+","+fav_dest_ids+","+fav_entmt_ids+"\n")
            users.close()


    def change_username(email, new_username, file="users.csv"):
        """Checks if there is a combination of the entered [email, password] in our user database 
        and changes the password in the list of our current user
        
        Args:
            email (str): Email(ID) of our user, used to identify which password to change
            new_username (str): The new_username will replace the old username that matches the users email(ID)
            file (str): Predetermined file of our stored users

        Returns:
            Nothing to return
        """
        user_information = StartupInterface.get_user_info(file)
        for user_creds in user_information:
            if user_creds[0] == email:
                user_creds[2] = str(new_username)
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w+")  # opening the file with w+ clears the file
        f.close()
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w") as users:
            '''Rewriting our users.csvfile with the updated username of the user'''
            users.write("email"+","+"password"+","+"username"+","+"fav_dest_ids"+","+"fav_entmt_ids"+"\n")
            for user_creds in user_information:
                email = user_creds[0]
                password = user_creds[1]
                username = user_creds[2]
                fav_dest_ids = user_creds[3]
                fav_entmt_ids = user_creds[4]
                users.write(email+","+password+","+username+","+fav_dest_ids+","+fav_entmt_ids+"\n")
                
            users.close()

    
    def delete_account(email, file="users.csv"):
        """Deletes all information about the user from our database including reviews
        
        Args:
            email (str): The email(ID) of the user we want to delete from our database
            file (str): Predetermined file which stores all of our users information

        Returns:
            Nothing to return
        """
        user_info = StartupInterface.get_user_info(file)
        count = 0
        for usercreds in user_info:
            count +=1
            if email == usercreds[0]:
                user_info.pop(count-1)
        f = open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w+") # opening the file with w+ clears the file
        f.close()
        with open(os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__))[:-12],"csvdata"), file), "w") as users:
            '''Rewriting our users.csvfile with the user deleted '''
            if user_info != []:
                users.write("email"+","+"password"+","+"username"+","+"fav_dest_ids"+","+"fav_entmt_ids"+"\n")
                for user_creds in user_info:
                    email = user_creds[0]
                    password = user_creds[1]
                    username = user_creds[2]
                    fav_dest_ids = user_creds[3]
                    fav_entmt_ids = user_creds[4]
                    users.write(email+","+password+","+username+","+fav_dest_ids+","+fav_entmt_ids+"\n")
                users.close()
            else:
                users.write("email"+","+"password"+","+"username"+","+"fav_dest_ids"+","+"fav_entmt_ids"+"\n")
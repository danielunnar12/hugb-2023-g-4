# READ ME

    "Travelator" is an application that should be able to help users plan their adventures when traveling to different locations. (In progress)

# Things you need to run the application

1. python, https://www.python.org/downloads/.
2. pip, https://pip.pypa.io/en/stable/installation/.
3. gRPC, after you download pip, in terminal $ python -m pip install grpcio (can be python or python3 depending on your version). (the dollar sign just represents terminal input so don't include it when running the command).
4. Two open terminal windows 

# How to run the application:

1. Download src
2. Open two terminal windows side by side
3. Set the paths of both of the terminal windows to Travelator within the src/python/Travelator folder
4. In the first terminal window enter $ python3 ./trav_server.py to start the server
5. In the second terminal window enter $ python3 ./trav_client.py to start the client
6. Then just play with the client


# Testing:

1. Test.py has all unit tests
2. Simply open up a terminal window and set your path to src/python/Travelator
3. For testing, simply run the file.
4. For Coverage, do $ coverage run -m unittest test.py (Make sure you do python/python3 -m if needed)
5. To get a review of the tests enter $ coverage report and statistic about the tests will be presented
6. You can see the test coverage for the main branch in the pipeline.

# Lab 14:20 - 16:00 29/08/2023 :
 4 arrived to the first Lab (Andri, Jakob, Daníel og Rúnar). We introduced ourselves to each other and discussed the project with our TA (Jón Róbert) about the course and plans going forward. We got GitLab set-up and made a Discord server for communication, We set-up plans for meetings in the future and went over the sprint details thoroughly.


# Meeting 10:10 - 13:40 31/08/2023 : 
Same 4 arrived, no news of the 3 missing. We started discussing what kind of application this is going to be and have come to an agreement (Travelator). We started creating Requirements, User Stories and a Project Overview.

# Lab 14:20 - 16:00 05/09/2023 : 
Three group members arrived to the Lab (Andri, Jakob and Rúnar), Daníel could not attend because he was recovering from an operation. The group talked to their TA and asked the prepared questions the group had. The TA clarified a lot of confusion and told the group that their was way too big, especially considering the group had three people fewer than the class planned for. So the group came up with a new project with a smaller scope and the TA was content and thought it doable. The group made new requirements and user stories that fit the new project.

# Meeting 10:10 - 14:00 09/09/2023 : 
Two members, Jakob and Rúnar, met up in HR while the other two, Andri and Daníel, were at home on discord. All members communicated through discord and found no issues in doing so. The group kept working on the user stories and requirements and also finished the project overview. Some  work was also put towards starting the domain model and the TA gave a lot of useful input that helped the group going forward.

# Meeting 10:10 - 13:00 11/09/2023 : 
All four members met up in HR. The project was due on this day so the group worked on finishing the domain model, the two scenarios and the user stories and connecting some requirements to those user stories.

# Lab 14:20 - 17:00 12/09/2023 : 
All four members arrived to the lab. The group started working on the second sprint. The group used a site that randomly decided which member was the designated scrum master and the site chose Jakob to be the scrum master. The group also used a planning poker site to estimate how much time each user story would take. The group added some requirements as tasks to some user stories. Finally the group agreed on a Definition of Done (DoD) but needed help from the TA to know where exactly to put the DoD.

# Meeting 10:10 - 13:20 14/09/2023 : 
All members arrived to the meeting. The group doesn't want to start coding yet until we know how gRPC works and how we should be coding. Meanwhile, the group works on ensuring all data would work by testing csv reading and sets up classes.

# Meeting 10:10 - 13:30 18/09/2023 : 
All members arrived to the meeting. The group started learning how to use gRPC with python. No actual coding has been done yet as we are still getting introduced to gRPC but the group has gotten a decent grasp.

# Lab 14:20 - 17:00 19/09/2023 : 
All members arrived to the lab. The group received feedback from Róbert(TA) on the last sprint submission. The feedback was positive and only a few changes needed such as moving all code to source, adding an issue board for tasks 'in process' and so on. The group asked Róbert a few questions about things that were unclear and he clarified some confusion. The group then finished the csv files that were going to be used and filled them with data. The rest of the meeting went into working with gRPC and understanding it better while also working on the Definition of Done and finishing the tasks listed there.

# Meeting 10:10 - 14:00 21/09/2023 : 
All members arrived to the meeting. The group transferred the time estimation for each user story from an excel file to the user stories on gitlab. The group divided some of the tasks amongst themselves and each member started working on their assigned task. The group also started looking into unit testing and test coverage to meet the demands of the sprints rubric. The group is on edge about leaving destinations for next sprint, to be decided on Monday.

# Meeting 10:10 - 15:05 25/09/2023 : 
All members arrived to the meeting. Destinations was finished alongside events but entertainment still needed work. For now destinations, events and entertainment are left out of the sprint.

# Meeting 20:30 - 23:00 25/09/2023 : 
All members arrived to the meeting. We worked on finishing touches to fill out the requirements on the rubric.

# Lab 14:20 - 16:30 26/09/2023 : 
All members arrived, Daníel was chosen as scrum master. 
1. How accurate were your estimates?
  1. We heavily underestimated how long it would take, but we got a decent 
     grip of the structure now
2. Did you plan too much or too little?
  2. Way too much was planned, we could only finish 2/7 user stories, but 
     the others were being implemented
3. Did anything change in your understanding of the product?
  3. At first we just thought it would be some simple and long code with 
     data management but now we see the product uses gRPC and it changed 
     our understanding from simple and long code to a fully fledged system
4. Do you need to update the product backlog based on this?
  4. Yes, it has to be updated as our estimations were wrong.
We went over our estimations and re-estimated them using planning poker again using what we have learned.

# Meeting 10:10-14:20 28/09/2023 :  
Andri, Jakob and Rúnar arrived to the meeting at school while Daníel was sick so he was working from home. Daníel was chosen as scrum master during the lab session two days prior so he gave the group that is going to be reviewing us access to our repository. Apart from that each member worked on their assigned tasks until some members went to the lecture on gRPC for those having trouble with it, so that they could gain better understanding on it.

# Meeting 10:10-14:00 2/10/2023 : 
Andri, Jakob and Rúnar arrived to the meeting at school while Daníel was sick so he was working from home. Róbert sent us an email about the review of the last sprint. There were some things to fix before the lab tomorrow so we worked on fixing them. Each member also worked on their own tasks, Jakob changed the logic to the server and we were added to group five´s repository so we can begin reviewing their code.

# Lab 14:20-16:30 3/10/2023  :  
Jakob and Rúnar arrived to school, Daníel listened on discord while Andri unfortunately did not make it. We had a short meeting with Róbert(TA) where we discussed what we had to do for the rest of this sprint. He told us to change a few things on Gitlab (mostly locations of code and other files) and also to fix our issue boards (moving issues to closed). We also discussed and got some clarity for our code reviews and put more stuff into our DoD for this sprint. After the meeting we worked on our assigned tasks.

# Meeting 10:10-14:00 5/10/2023 : 
Jakob and Rúnar arrived to school, Daníel was on discord and Andri did not attend but was going to work on things later this day. Jakob and Daníel spent time trying to fix imports issues with grpc_pb2 files which has been very time consuming and has haltered our work in a big way which means we might need to go back to our old directory setup. Jakob and Daníel then started on getting get_destinations to work through grpc. Jakob finished his code review for group 5 and Rúnar started his code review.

# Meeting 10:10-14:00 9/10/2023 : 
Andri, Daníel and Jakob arrived to school, Rúnar was sick so he stayed home but was going to try and finish his work with entertainments. Daníel, Andri and Jakob finished all functions for Destinations and Events. We will attempt to finish adding the functionality to the client later tonight and our user testing is getting at least 85% coverage at this time. Code reviews are mostly finished. We have also made the decision to postpone adding favorite location functionality and are uncertain about the progress on entertainments/can't be guaranteed to be finished by today.

# Lab 14:20 - 16:00 10/10/2023 : 
Andri, Jakob and Rúnar showed up to the lab meeting. Daníel could not show up to the lab meeting because he took a covid-19 test and found out he had covid. The group decided it would be best if Andri was the scrum master because Rúnar had a trip planned and so it would not be ideal if Rúnar was the scrum master. The group agreed that another planning poker was unnecessary as the time to implement each user story would be the same as agreed on in sprint 3. Some fixing had to be done to the code, mainly the events part, because some files were missing. 

# Meeting 10:10-12:45 16/10/2023 : 
Andri, Daníel and Jakob met up on the third floor of HR as per usual. Rúnar could not show up as he was on a trip. The group worked on organizing the code and files, which was a hassle because the group had to change all paths and imports so the code would work as intended. The group also implemented both the favorite location user story as well as the weather for each location task.

# Lab 14:20 - 17:00 17/10/2023 : 
The whole group showed up to the lab. The group continued work on different aspects of the project. The group only had one question for the TA, how should we test the weather station functions or should we even test them at all?, the TA had a good suggestion for how the group should do this but also pointed out the group only had to have a coverage of 50% so the group decided not to test the weather station. 

# Meeting 10:10-14:30 19/10/2023 : 
All group members showed up to the third floor of HR. The group continued working on the project with only the favorite locations and entertainment user stories left for the DoD to be met. The group finished the favorite locations user story in during the meeting with just some fine tuning having to be done and the entertainment user story was also close to being done.

# Meeting 10:10-13:00 23/10/2023 : 
Jakob, Rúnar and Daníel showed up to the meeting, Andri was working from home due to feeling a bit sick. The group made some final adjustments for turning in sprint 4 tonight.

# Lab 14:20-17:00 24/10/2023 : 
Jakob, Daníel and Rúnar showed up to the lab but unfortunately Andri was sick at home. In this lab we had a short meeting with the TA as there were not many questions from us. The review of sprint 3 got pushed back so we are hoping to receive it in the next lab. Rúnar was chosen as the Scrum master as he is the only one that has not done it yet. We decided on how we should implement the calendar while also making the DoD for this last sprint.

# Meeting 10:10-14:00 26/10/2023 : 
The whole group showed up to the meeting. During the meeting group members tried to figure out the CI/CD part of the sprint but decided to wait until Grischa finished the next presentation because he said he would talk about how to implement and work with CI/CD. Other group members worked on finishing the calendar and reviews. The group came to the conclusion that it is highly probable that the more advanced filters will not be finished before the deadline of the project and the group will likely remove those user stories.

# Meeting 10:10-13:00 30/10/2023 :
The whole group showed up to the meeting. The group has decided to add the accessibility filters related to age, disability and drinking to the DoD. We are still waiting on the lecture on CI/CD, otherwise members are just continuing work and fixing what Róbert said was wrong for sprint 3 and 4. This means updating readme and moving the protocol that has been collected in wiki to this document.

# Lab 14:20-16:00 31/10/2023 : 
The whole group showed up to this lab. We had a meeting with the TA and went over the last two sprints and what went wrong. In conclusion we forgot to comment and also to move the decision protocol into the main branch. There was also some trouble for the TA running tests because we didn´t change the README file. During the lab we changed a few things at the banch for the TA, mostly commenting our code and at the end of the session the TA looked at the main branch again. We also worked on the our tasks, fixed a few errors and started experimenting with the CI/CD pipeline.

# Meeting 10:10-13:30 2/11/2023 :
Andri, Jakob and Rúnar met up at school while Daníel could not make it. Today we kept on experimenting and working with the pipeline and we´ve made some good progress. Daníel is in charge of the calendar and he is working on that. Apart from those tasks we did some fixes, the event and entertainment filter had some errors and also a few print statements that were all fixed. Apart from those we kept on trying to finish the DoD and make the system ready for the last handin. 

# Meeting 10:10-23:30 6/11/2023 :
Today´s meeting was a little different, all members arrived but we were working on and off for the entire day from home on discord. There was also some work done over the weekend individually. Today we worked on finishing the project. We did that by finishing the DoD, implementing the calendar, removing the Main folder from the code as the TA said it was unnecessary while also making tests for the code, working on the pipeline and getting coverage analysis on all branches. We also fixed bugs and errors to make the system presentable and high in quality for the final handin.
